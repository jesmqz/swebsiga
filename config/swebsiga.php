<?php
  return [
    'domain_ws' => env('DOMAIN_WS'),
    'academia_ws' => env('ACADEMIA_WS'),
    'virtual_ws' => env('VIRTUAL_WS'),
    'campus_url' => env('CAMPUS_URL')
  ];
