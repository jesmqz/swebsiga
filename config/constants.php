<?php 
return [
    'state' => [
        'UNPROCCESED' => '0',
        'SPACECREATED' => '1',
        'ENROLLEDCREATED' => '2',
        'UPDATING' => '3',
        'VALIDATED' => '4',
        'PAUSED' => '5',
        'ERROR' => '6',
        'REQUESTUPDATING' => '7', 
        'UNENROLLING' => '8'
    ]
];