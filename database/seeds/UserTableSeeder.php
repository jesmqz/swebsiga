<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_guess = Role::where('name', 'guess')->first();
        $role_admin = Role::where('name', 'admin')->first();
        
        $user = new User();
        $user->name = 'Jesus Marquez';
        $user->email = 'jamarquez@uao.edu.co';
        $user->password = bcrypt('Cit.2018');
        $user->save();
        $user->roles()->attach($role_admin);
        
        $user = new User();
        $user->name = 'Yuly Cordoba';
        $user->email = 'ycordoba@uao.edu.co';
        $user->password = bcrypt('Cit.2018');
        $user->save();
        $user->roles()->attach($role_admin);

        $user = new User();
        $user->name = 'Cristhian Restrepo';
        $user->email = 'carestrepo@uao.edu.co';
        $user->password = bcrypt('Cit.2018');
        $user->save();
        $user->roles()->attach($role_admin);

        $user = new User();
        $user->name = 'Juan Cardona';
        $user->email = 'jjcardonaq@uao.edu.co';
        $user->password = bcrypt('Cit.2018');
        $user->save();
        $user->roles()->attach($role_admin);

        $user = new User();
        $user->name = 'German Gallego';
        $user->email = 'ggallego@uao.edu.co';
        $user->password = bcrypt('Cit.2018');
        $user->save();
        $user->roles()->attach($role_admin);

        $user = new User();
        $user->name = 'Guess';
        $user->email = 'jesmqz@gmail.com';
        $user->password = bcrypt('12345');
        $user->save();
        $user->roles()->attach($role_guess);
    }
}
