<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicituds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('periodo', 12)->nullable($value = false);
            $table->string('username', 100)->nullable($value = false);
            $table->string('codigo_asignatura', 24)->nullable($value = false);
            $table->char('modalidad', 1)->nullable($value = false);
            $table->char('state', 1)->nullable($value = false);
            $table->timestamp('fecha_solicitud');
            $table->timestamp('fecha_procesamiento')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicituds');
    }
}
