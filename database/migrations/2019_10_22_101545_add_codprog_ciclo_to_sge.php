<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodprogCicloToSge extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solicitud_grupos_estudiante', function (Blueprint $table) {
            $table->string('cod_prog', 3)->nullable($value = false)->after('username');
            $table->string('ciclo', 240)->nullable($value = false)->after('cod_prog');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitud_grupos_estudiante', function (Blueprint $table) {
            $table->dropColumn(['cod_prog'],['ciclo']);
        });
    }
}
