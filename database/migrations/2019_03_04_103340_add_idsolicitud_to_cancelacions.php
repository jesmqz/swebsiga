<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdsolicitudToCancelacions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cancelacions', function (Blueprint $table) {
            $table->unsignedInteger('solicitud_id')->nullable($value=false)->after('codigo_asignatura');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cancelacions', function (Blueprint $table) {
            $table->dropColumn(['solicitud_id']);
        });
    }
}
