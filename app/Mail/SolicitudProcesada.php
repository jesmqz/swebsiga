<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SolicitudProcesada extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Datos de la solicitud 
     */
    public $solicitud;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($s)
    {
        //
        $this->solicitud = $s;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Activación de curso en campus UAO Virtual')->markdown('emails.solicitudprocesada');
    }
}
