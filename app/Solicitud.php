<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
    public function grupos()
    {
        return $this->hasMany('App\Grupo');
    }
    public function cancelaciones()
    {
        return $this->hasMany('App\Cancelacion');
    }
}
