<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class solicitud_grupo_estudiante extends Model
{
    //
    protected $table = 'solicitud_grupos_estudiante';

    public function grupo()
    {
        return $this->belongsTo('App\Grupo');
    }
}
