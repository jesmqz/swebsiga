<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cancelacion extends Model
{
    //
    public function solicitud() 
    {
        return $this->belongsTo('App\Solicitud');
    }
}
