<?php

namespace App\Http\Controllers;

use App\Solicitud;
use App\Grupo;
use App\solicitud_grupo_estudiante;
use Illuminate\Http\Request;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException;
use App\Parametro;
use Illuminate\Support\Facades\DB;

class SolicitudController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['create', 'store']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // index
        // buscar periodo actual
        $collection_parametro = Parametro::where('nombre', 'periodo-actual')->get();
        foreach ($collection_parametro as $parametro) {
          $ok = false;
        }
        // buscar soliciudes del perido actual
        $solicitudes = Solicitud::where('periodo', $parametro->valor)->get();

        return view('solicitudes.index', ['solicitudes' => $solicitudes, 'periodo_actual' => $parametro->valor ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // obtenemos el periodo para las solicitudes
        $collection_parametro = Parametro::where('nombre', 'periodo-solicitud')->get();
        foreach ($collection_parametro as $parametro) {
            $ok = false;
        }
        // obtenemos email y codigo de asignatura
        $email = $request->input('email');
        $codigoAsignatura = $request->input('codigo');

        // obtenemos el username del docente
        $emailArray = explode("@", $email);
        $username = strtolower($emailArray[0]);
        
        $docente_not_found = false;
        // obtenemos datos del docente
        $client = new \GuzzleHttp\Client();

        $domainws = config('swebsiga.domain_ws');
        $ws_academia = config('swebsiga.academia_ws');

        try {
            $response_docente = $client->request('GET', $domainws.$ws_academia.'/api/docente/'.$username);
        } catch (ClientException $e) {
            // $status_cod_error = Psr7\str($e->getResponse());
            $response = $e->getResponse();
            $body = $response->getBody();
            $str_body = (string) $body;
            $body_php = json_decode($str_body, TRUE);
            // si se quiere evitar a docentes solo de la vista oracle
            /* 
            if ($response->getStatusCode() == 404) {
                $request->session()->flash('message', $body_php['data']['message']);
            } else {
                $request->session()->flash('message', $str_body);
            }
            
            return redirect()->route('consultarGruposAsignaturaDocente');
            */
            $docente_not_found = true;
        }

        if ($docente_not_found) {
            $docente = array("username" => $username, "firstname" => $username, "lastname" => $username ); 
        } else {
            $status_code_docente = $response_docente->getStatusCode();
            $body_docente = $response_docente->getBody();
            $body_docente_array = json_decode($body_docente, TRUE);

            $docente = array("username" => $username, "firstname" => $body_docente_array['data']['firstname'], "lastname" => $body_docente_array['data']['lastname'] ); 
        }

        // obtenemos datos de la asignatura
        try {
            $response_asignatura = $client->request('GET', $domainws.$ws_academia.'/api/asignatura/'.$codigoAsignatura);
        } catch(ClientException $e) {
            $response = $e->getResponse();
            $body = $response->getBody();
            $str_body = (string) $body;
            $body_php = json_decode($str_body, TRUE);
            
            if ($response->getStatusCode() == 404) {
                $request->session()->flash('message', $body_php['data']['message']);
            } else {
                $request->session()->flash('message', $str_body);
            }

            return redirect()->route('consultarGruposAsignaturaDocente');
        }
        $status_code_asignatura = $response_asignatura->getStatusCode();
        $body_asignatura = $response_asignatura->getBody();
        $body_asignatura_array = json_decode($body_asignatura, TRUE);


        // obtenemos los grupos de la asignatura segun valor parametro periodo-solicitud
        try {
            $response_grupos = $client->request('GET', $domainws.$ws_academia.'/api/grupos/'.$codigoAsignatura.'/'.$parametro->valor);
        } catch(ClientException $e) {
            $response = $e->getResponse();
            $body = $response->getBody();
            $str_body = (string) $body;
            $body_php = json_decode($str_body, TRUE);

            if ($response->getStatusCode() == 404) {
                $request->session()->flash('message', $body_php['data']['message']);
            } else {
                $request->session()->flash('message', $str_body);
            }

            return redirect()->route('consultarGruposAsignaturaDocente');
        }

        $status_code_grupos = $response_grupos->getStatusCode();
        $body_grupos = $response_grupos->getBody();
        $body_grupos_array = json_decode($body_grupos, TRUE);

        // buscar solicitudes asociadas con la asignatura y periodo-solicitud configurado
        $solicitudes = Solicitud::where(['codigo_asignatura' => $body_asignatura_array['data']['codigo'], 'periodo' => $parametro->valor])->get();
        
        return view('solicitudes.createsolicitud', ['docente' => $docente, 'asignatura' => $body_asignatura_array['data'], 'grupos' => $body_grupos_array['data']['grupos'], 'solicitudes' => $solicitudes] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // obtenemos el periodo fijado para las solicitudes
        $collection_parametro = Parametro::where('nombre', 'periodo-solicitud')->get();
        foreach ($collection_parametro as $parametro) {
            $ok = false;
        }
    
        $time_unix_current = time();
        // obtengo grupos
        $grupos = $request->input('grupos.*');
        // determinar si hay cursos virtuales
        $mark_virtual = "-VIRT";
        $solicitud_virtual = false;
        $solicitud_blearning = false;

        // here I verify whether the request has groups elearning and/or belearning
        foreach ($grupos as $grupo) {
            if (strlen($grupo) > 5) {
                $substr_5 = substr($grupo, -5);
                if ($mark_virtual === $substr_5) {
                    $solicitud_virtual = true;
                } else {
                    // si el grupo es mas de 5 caracteres pero no es virtual
                    $solicitud_blearning = true;
                }
            } else {
                $solicitud_blearning = true;
            }
        }

        // if request has belearning mode then process it
        if ($solicitud_blearning) {
            // if teacher has already a request with same subject then add the group to the request
            $requested = Solicitud::where('periodo', $parametro->valor)
                                        ->where('username', $request->get('username'))
                                        ->where('codigo_asignatura', $request->get('cod_asignatura'))
                                        ->where('modalidad', 'B')
                                        ->get();

            if ($requested->isEmpty()) {
                // generate new request type belearning
                $solicitudBlearning = new Solicitud;
                $solicitudBlearning->periodo = $parametro->valor;
                $solicitudBlearning->username = $request->get('username');
                $solicitudBlearning->codigo_asignatura = $request->get('cod_asignatura');
                $solicitudBlearning->modalidad = 'B';
                $solicitudBlearning->facultad = $request->get('facultad');
                $solicitudBlearning->departamento = $request->get('departamento');
                $solicitudBlearning->fecha_solicitud = date("Y-m-d H:i:s", $time_unix_current);
                $solicitudBlearning->state = "0";
                $solicitudBlearning->save();
                // I store this request's id for use it as relationship in groups
                $request_id = $solicitudBlearning->id;
            } else {
                foreach ($requested as $r) {
                    $ok = false;
                }
                // I store this request's id for use it as relationship in groups
                $request_id = $r->id;
            }

            foreach ($grupos as $grupo) {
                $nuevoGrupo = new Grupo;
                $nuevoGrupo->grupo = $grupo;
                // asociar grupos con tipo de modalidad solicitud
                if (strlen($grupo) > 5) {
                    $substr_5 = substr($grupo, -5);
                    if ($mark_virtual === $substr_5) {
                        // it is virtual, it skip
                        continue;
                    } else {
                        // es curso blearning
                        $nuevoGrupo->solicitud_id = $request_id;
                    }
                } else {
                    // es curso blearning
                    $nuevoGrupo->solicitud_id = $request_id;
                }
                $nuevoGrupo->save();
            }
        }

        // if request has belearning mode then process it
        if ($solicitud_virtual) {
            // if teacher has already a request with same subject then add the group to the request else create new request
            $requested = Solicitud::where('periodo', $parametro->valor)
                                        ->where('username', $request->get('username'))
                                        ->where('codigo_asignatura', $request->get('cod_asignatura'))
                                        ->where('modalidad', 'E')
                                        ->get();


            // generar solcitud virtual
            if ($requested->isEmpty()) {
                $solicitudVirtual = new Solicitud;
                $solicitudVirtual->periodo = $parametro->valor;
                $solicitudVirtual->username = $request->get('username');
                $solicitudVirtual->codigo_asignatura = $request->get('cod_asignatura');
                $solicitudVirtual->modalidad = 'E';
                $solicitudVirtual->facultad = $request->get('facultad');
                $solicitudVirtual->departamento = $request->get('departamento');
                $solicitudVirtual->fecha_solicitud = date("Y-m-d H:i:s", $time_unix_current);
                $solicitudVirtual->state = "0";
                $solicitudVirtual->save();
                // I store this request's id for use it as relationship in groups
                $request_id = $solicitudVirtual->id;
            } else {
                foreach ($requested as $r) {
                    $ok = false;
                }
                // I store this request's id for use it as relationship in groups
                $request_id = $r->id;
            }

            foreach ($grupos as $grupo) {
                $nuevoGrupo = new Grupo;
                $nuevoGrupo->grupo = $grupo;
                // asociar grupos con tipo de modalidad solicitud
                if (strlen($grupo) > 5) {
                    $substr_5 = substr($grupo, -5);
                    if ($mark_virtual === $substr_5) {
                        // es virtual
                        $nuevoGrupo->solicitud_id = $request_id;
                    } else {
                        // it is belearning, it skip
                        continue;
                    }
                } else {
                    // it is belearning, it skip
                    continue;
                }
                $nuevoGrupo->save();
            }
        }

        return view('solicitudes.successtore', ['grupos' => $grupos]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Solicitud  $solicitud
     * @return \Illuminate\Http\Response
     */
    public function show(Solicitud $solicitud)
    {
        // show solicitudes
        return view('solicitudes.deletesolicitud', ['solicitud' => $solicitud]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $solicitud = Solicitud::find($id);
        return view('solicitudes.editsolicitud', ['solicitud' => $solicitud]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Solicitud  $solicitud
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_solicitud)
    {
        $request->validate([
            'id' => 'required',
            'username' => 'required',
            'codigo_asignatura' => 'required',
            'modalidad' => 'required',
            'facultad' => 'required',
            'departamento' => 'required',
            'state' => 'required'
        ]);

        $solicitud = Solicitud::find($id_solicitud);
        $solicitud->state = $request->get('state');
        $solicitud->save();

        return redirect()->route('solicitudes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Solicitud  $solicitud
     * @return \Illuminate\Http\Response
     */
    public function destroy(Solicitud $solicitud)
    {
        //
        $grupos = $solicitud->grupos;
        $strGrupo = '';
        $strEstudiantes = '';
        foreach($grupos as $g) {
            $strGrupo.= ' id_grupo:'.$g->id .' name-grupo:'.$g->grupo;
            $deletedRows = solicitud_grupo_estudiante::where('grupo_id', $g->id)->delete();

            $g->delete();
        }
        $solicitud->delete();
        // return var_dump('id-solicitud:'.$solicitud->id . $strGrupo . 'estudiantes: '.$strEstudiantes);
        return redirect()->route('solicitudes');
    }

    public function showGroup($id_group) 
    {
        $group = Grupo::where('id', $id_group)->first();

        $studentSorted = $group->estudiantes->sortBy('username');
        return view('solicitudes.groupsolicitud', ['group' => $group, 'students' => $studentSorted]);
    }

    public function destroyGroup($id_group)
    {
        // search group
        $group = Grupo::find($id_group);
        // get request id
        $id_solicitud = $group->solicitud_id;
        // groups related with request
        $groups_solicitud = Grupo::where('solicitud_id', $id_solicitud);
        $groups_counts = $groups_solicitud->count();

        // delete students and group
        $deletedRows = solicitud_grupo_estudiante::where('grupo_id', $id_group)->delete();
        $groupDeleted = Grupo::where('id', $id_group)->delete();

        // if request now haven't groups then deleting request
        if ($groups_counts === 1) {
            $solicitudDeleted = Solicitud::where('id', $id_solicitud)->delete();
        }

        return redirect()->route('solicitudes');
    }

    public function redirectToCampus($shortname) 
    {
        $domainws = config('swebsiga.domain_ws');
        $ws_virtual = config('swebsiga.virtual_ws');
        $campus_url = config('swebsiga.campus_url');

        $client = new \GuzzleHttp\Client();
        try {
            $response_curso = $client->request('GET', $domainws.$ws_virtual.'/api/curso/'.$shortname);

            $status_code_curso = $response_curso->getStatusCode();
            $body_curso = $response_curso->getBody();
            $body_curso_array = json_decode($body_curso, TRUE);
            // obtenemos courseid
            $courseid = $body_curso_array["data"]["id"];

            $url_campus = $campus_url."/course/view.php?id=".$courseid;
            return redirect()->away($url_campus);
        } catch (ClientException $e) {
            $response = $e->getResponse();
            $body = $response->getBody();
            $str_body = (string) $body;
            $body_php = json_decode($str_body, TRUE);
            // saltamos la solicitud
            return view('solicitudes.courseisnot', ['shortname' => $shortname]);
        }
    }
}
