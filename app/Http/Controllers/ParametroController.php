<?php

namespace App\Http\Controllers;

use App\Parametro;
use Illuminate\Http\Request;

class ParametroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $parametros = Parametro::all();

        return view('parametros.index', ['parametros' => $parametros]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('parametros.createparametro');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $newParametro = new Parametro;
        $newParametro->nombre = $request->get('nombre');
        $newParametro->valor = $request->get('valor');
        $newParametro->save();

        $request->session()->flash('message', '!Parámetro guardado!');
        return redirect()->route('parametros');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Parametro  $parametro
     * @return \Illuminate\Http\Response
     */
    public function show(Parametro $parametro)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Parametro  $parametro
     * @return \Illuminate\Http\Response
     */
    public function edit(Parametro $parametro, $nombre)
    {
        // get parameter
        $collection = Parametro::where('nombre', $nombre)->get();
        foreach ($collection as $parametro) {
          $ok = false;
        }
        
        // if parameter is date type then
        if ( $parametro->nombre == 'hora-exec-solicitudes' || $parametro->nombre == 'hora-exec-updates' || $parametro->nombre == 'hora-exec-unenrolls') {
            if ($parametro->valor == 'none') {
                $time = array('00','00');
                $ampm = 'am';
                $disabled = true;
            } else {
                $time = explode(":", $parametro->valor);
                // to change time formar from 24h to 12h
                $ampm = 'am';
                if ($time[0] > 12) {
                    if ($time[0] == 24) {
                        $ampm = 'am';    
                    } else {
                        $ampm = 'pm';
                    }
                    $new_hour = $time[0] - 12; 
                    $new_hour_str = '0'.$new_hour;
                    $time[0] = $new_hour_str;
                }
                $disabled = false;
            }
            $p = array('nombre' => $parametro->nombre, 'valor' => $time, 'ampm' => $ampm, 'disabled' => $disabled);        
        } else {
            $p = array('nombre' => $parametro->nombre, 'valor' => $parametro->valor);
        }
        return view('parametros.editparametro',['parametro' => $p]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Parametro  $parametro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Parametro $parametro, $nombre)
    {
        if ($nombre == 'hora-exec-solicitudes' || $nombre == 'hora-exec-updates' || $nombre == 'hora-exec-unenrolls') {
            if ($request->get('disabled') == false) {
                // it does change time
                $hour = $request->get('hour');
                if ($request->get('amfm') == 'pm') {
                    $hour = $hour + 12;
                } else {
                    if ($hour == 12) {
                        $hour = 24;
                    }
                }
                $nuevo_valor = $hour.':'.$request->get('minute');
            }
            else {
                $nuevo_valor = 'none';
            }
        } else {
            $nuevo_valor = $request->get('valor');
        }
 
        // it does search parameter
        $collection = Parametro::where('nombre', $nombre)->get();
        foreach ($collection as $parametro) {
          $ok = false;
        }

        $parametro->nombre = $request->get('nombre');
        $parametro->valor = $nuevo_valor;
        $parametro->save();

        $request->session()->flash('message', '!Parámetro actualizado!');
        return redirect()->route('parametros');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Parametro  $parametro
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Parametro $parametro, $nombre)
    {
      $user = Parametro::where('nombre', $nombre)->first();
      $user->delete();
      
      $request->session()->flash('message', '¡Parámetro eliminado satisfactoriamente!');

      return redirect()->route('parametros');
    }
}
