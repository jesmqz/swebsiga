<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Hash;
use App\Parametro;
use App\Solicitud;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['consultarGruposAsignaturaDocente', 'redirigirACrearSolicitud']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $domainws = config('swebsiga.domain_ws');
        $ws_academia = config('swebsiga.academia_ws');

        $request->user()->authorizeRoles(['guess', 'admin']);

        // obtener periodo académico actual
        $collection_parametro = Parametro::where('nombre', 'periodo-actual')->get();
        if ($collection_parametro->isEmpty()) {
            $periodo_actual = 'none';
        } else {
            foreach ($collection_parametro as $parametro) {
                $periodo_actual = $parametro->valor;
                $ok = false;
            }
        }

        // obtener solicitudes del período actual 
        $solicitudes = Solicitud::where('periodo', $periodo_actual)->get();

        // contar solicitudes
        $total_solicitudes = $solicitudes->count();

        //filtrar solicitudes sin procesar
        $solicitudes_por_procesar = $solicitudes->filter(function ($value, $key) {
          return $value->state == '0';
        });

        //filtrar solicitudes en updating
        $solicitudes_updating = $solicitudes->filter(function ($value, $key) {
          return $value->state == '3';
        });  
        
        //filtrar solicitudes ok
        $solicitudes_ok = $solicitudes->filter(function ($value, $key) {
          return $value->state == '4';
        });  
        
        //filtrar solicitudes en estado pausado
        $solicitudes_pausadas = $solicitudes->filter(function ($value, $key) {
          return $value->state == '5';
        });

        //filtrar solicitudes en estado !
        $solicitudes_admiracion = $solicitudes->filter(function ($value, $key) {
          return $value->state == '6';
        });

        //filtrar solicitudes en estado update
        $solicitudes_update = $solicitudes->filter(function ($value, $key) {
          return $value->state == '7';
        });
                
        $total_solicitudes_por_procesar = $solicitudes_por_procesar->count();
        $total_solicitudes_ok = $solicitudes_ok->count();
        $total_solicitudes_updating = $solicitudes_updating->count();
        $total_solicitudes_pausadas = $solicitudes_pausadas->count();
        $total_solicitudes_update = $solicitudes_update->count();
        $total_solicitudes_admiracion = $solicitudes_admiracion->count();

        $totales = array('total_solicitudes_por_procesar' => $total_solicitudes_por_procesar,
                          'total_solicitudes_ok' => $total_solicitudes_ok,
                          'total_solicitudes_updating' => $total_solicitudes_updating,
                          'total_solicitudes_pausadas' => $total_solicitudes_pausadas,
                          'total_solicitudes_update' => $total_solicitudes_update,
                          'total_solicitudes_admiracion' => $total_solicitudes_admiracion,
                          'total_solicitudes' => $total_solicitudes
                        );
        /*
        $num_solicitudes_validada = Solicitud::where('periodo', $periodo_actual)->where('state', '4')->count();
        $num_solicitudes_sin_procesar = Solicitud::where('periodo', $periodo_actual)->where('state', '0')->count();
        */
        $num_cursos_elearning = Solicitud::where('periodo', $periodo_actual)->where('modalidad', 'E')->count();
        $num_cursos_blearning = Solicitud::where('periodo', $periodo_actual)->where('modalidad', 'B')->count();

        // renderizar home
        return view('home',['periodo' => $periodo_actual, 
                    'num_cursos_e' => $num_cursos_elearning, 
                    'num_cursos_b' => $num_cursos_blearning, 
                    'totales' => $totales]);
    }

    public function admin(Request $request)
    {
      return view('administration');
    }

    public function users(Request $request)
    {
      $users = DB::select('select * from users');
      
      return view('users.index', ['users' => $users]);
    }
    
    public function createUser(Request $request)
    {
        return view('users.createuser');
    }

    public function deleteUser(Request $request, $email)
    {
      $user = User::where('email', $email)->first();
      $user->delete();
      
      $request->session()->flash('message', '¡Usuario eliminado satisfactoriamente!');
      
      // return response('success user deleted', 200);
      
      return redirect()->route('users');
    }

    public function editUser(Request $request, $email)
    {
      $users = DB::select('select * from users where email = :email', ['email' => $email]);
      foreach($users as $u) {
        $user = $u;

      }

      return view('users.edituser', compact('user'));
    }

    public function saveUser(Request $request)
    {
      $validatedData = $request->validate([
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255|unique:users',
        'password' => 'required|string|min:6|confirmed'
      ]);

      $user = $request->user();

      $newUser = new User;
      $newUser->name = $request->get('name');
      $newUser->email = $request->get('email');
      $newUser->password = Hash::make($request->get('password'));
      $newUser->save();

      $newUser
      ->roles()
      ->attach(Role::where('name', 'guess')->first());

      $request->session()->flash('message', '¡Usuario guardado satisfactoriamente!');
      return redirect()->route('users');
    }

    public function updateUser(Request $request, $email)
    {
      $validatedData = $request->validate([
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255',
        'password' => 'required|string|min:6|confirmed'
      ]);

      $newName = $request->get('name');
      $newEmail = $request->get('email');
      $newPassword = Hash::make($request->get('password'));

      $affected = DB::update('update users set name = ?, email = ? , password = ? where email = ?', [$newName, $newEmail, $newPassword, $email]);
      
      $request->session()->flash('message', '¡Usuario actualizado satisfactoriamente!');
      return redirect()->route('users');
    }

    public function consultarGruposAsignaturaDocente() {

      // obtener periodo establecido en el parametro periodo-solicitud
      $collection_parametro = Parametro::where('nombre', 'periodo-solicitud')->get();
      if ($collection_parametro->isEmpty()) {
        return '!Periodo para solicitudes no está configurado!';
      } else {
        foreach ($collection_parametro as $parametro) {
            $ok = false;
        }
      }
      // mostrar formulario para obtener email y asignatura paso 1
      return view('solicitudes.consultargruposasignaturadocente', ['periodo' => $parametro->valor]);
    }

    public function redirigirACrearSolicitud(Request $request) {
      $messages = [
        'codigo.max' => 'Código de asignatura inválido',
      ];

      $validator = Validator::make($request->all(), [
        'email' => 'required|string|email|max:255',
        'codigo' => 'required|string|max:15',
      ], $messages)->validate();
    
      $email = $request->input('email');
      $codigoAsignatura = $request->input('codigo');
      // redirigir al formulario para crear la solicitud
      // return Route::view('solicitud/create', 'createsolicitud', ['email' => $email, 'codigo' => $codigoAsignatura]);
      return redirect()->route('solicitud.create', ['email' => $email, 'codigo' => $codigoAsignatura]);
    }
  }
