<?php

namespace App\Http\Controllers;

use App\Periodo;
use App\Parametro;
use Illuminate\Http\Request;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\DB;

class PeriodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // index
        $periodos = Periodo::all();

        return view('periodos.index', ['periodos' => $periodos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $client = new \GuzzleHttp\Client();

        $domainws = config('swebsiga.domain_ws');
        $academiaws = config('swebsiga.academia_ws');

        // obtenemos los periodos
        try {
            $response_periodos = $client->request('GET', $domainws.$academiaws.'/api/periodos');
        } catch(ClientException $e) {
            $response = $e->getResponse();
            $body = $response->getBody();
            $str_body = (string) $body;
            $body_php = json_decode($str_body, TRUE);

            if ($response->getStatusCode() == 404) {
                $request->session()->flash('message', $body_php['data']['message']);
            } else {
                $request->session()->flash('message', $str_body);
            }
            return redirect()->route('periodos');
        }

        $status_code_periodos = $response_periodos->getStatusCode();
        $body_periodos = $response_periodos->getBody();
        $body_periodos_array = json_decode($body_periodos, TRUE);


        $collection = Parametro::where('nombre', 'periodo-actual')->get();
        foreach ($collection as $parametro) {
          $ok = false;
        }
        if ($parametro->valor == 'none') {
            return view('periodos.createperiodo', ['periodos' => $body_periodos_array['data']['periodos'] ]);
        } else {
            // seleccionar los ultimos cuatro períodos
            $total_periodos = count($body_periodos_array['data']['periodos']);
            $last_periodos = array();
            // switche para ubicar la posición del periodo actual
            $sw = false;
            for($i=1; $i <= $total_periodos; $i++) {
                if ($sw) {
                    $last_periodos[] = $body_periodos_array['data']['periodos'][$i-1];
                } else {
                    if ($body_periodos_array['data']['periodos'][$i-1] == $parametro->valor) $sw = true;
                }
            }
        }
        return view('periodos.createperiodo', ['periodos' => $last_periodos ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $request->validate([
          'nombre' => 'required|string|max:12',
        ]);

        $newPeriodo = new Periodo;
        $newPeriodo->nombre = $request->get('nombre');
        $newPeriodo->save();

        $affected = DB::update('update parametros set valor = ? where nombre = ?', [$request->get('nombre'), 'periodo-actual']);

        $request->session()->flash('message', '¡Período guardado!');
        return redirect()->route('periodos');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Periodo  $periodo
     * @return \Illuminate\Http\Response
     */
    public function show(Periodo $periodo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Periodo  $periodo
     * @return \Illuminate\Http\Response
     */
    public function edit(Periodo $periodo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Periodo  $periodo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Periodo $periodo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Periodo  $periodo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Periodo $periodo)
    {
        //
    }
}
