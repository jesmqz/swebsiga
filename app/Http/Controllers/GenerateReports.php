<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use App\Solicitud;
use App\Parametro;
use App\solicitud_grupo_estudiante;
use App\Periodo;

use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Client;

class GenerateReports extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // obtener periodo académico actual
        $collection_parametro = Parametro::where('nombre', 'periodo-actual')->get();
        if ($collection_parametro->isEmpty()) {
            $current_period = 'none';
        } else {
            foreach ($collection_parametro as $parametro) {
                $current_period = $parametro->valor;
                $ok = false;
            }
        }

        // obtener periodos
        $periods = DB::table('periodos')
            ->where('nombre', 'like', '%-01%')
            ->orWhere('nombre', 'like', '%-03%')
            ->orderBy('id', 'desc')
            ->limit(4)
            ->get();

        // obtener facultades
        $faculties = DB::table('solicituds')->select('facultad')->distinct()->where('periodo', $current_period)->where('state', Config::get('constants.state.VALIDATED'))->get();

        // get faculties and his departaments
        $faculties_departments = DB::table('solicituds')->select('facultad','departamento')->distinct()->where('periodo', $current_period)->where('state', Config::get('constants.state.VALIDATED'))->orderBy('facultad', 'ASC')->get();

        // get faculties his departaments and his subjects
        $faculties_departments_subjects = DB::table('solicituds')->select('facultad','departamento', 'codigo_asignatura')->distinct()->where('periodo', $current_period)->where('state', Config::get('constants.state.VALIDATED'))->orderBy('facultad', 'ASC')->get();

        // create arrays for totalizing and counting
        foreach($faculties as $faculty) {
            $total_courses_by_faculty_elearning[$faculty->facultad] = 0;
            $total_courses_by_faculty_blearning[$faculty->facultad] = 0;
            
            $total_students_by_faculty_elearning[$faculty->facultad] = 0;
            $total_students_by_faculty_blearning[$faculty->facultad] = 0;
            $total_subjects_by_faculty[$faculty->facultad] = 0;
        }
        
        // create arrays for count subjects by departments 
        foreach ($faculties_departments as $fd) {
            $total_subjects_by_department[$fd->facultad][$fd->departamento] = 0;
        }

        // it count subject total by departments and by faculty
        foreach ($faculties_departments_subjects as $subject) {
            $total_subjects_by_department[$subject->facultad][$subject->departamento]++;
            $total_subjects_by_faculty[$subject->facultad]++;
        }

        // obtener solicitudes validadas del período actual 
        $requests = Solicitud::where('periodo', $current_period)->where('state', Config::get('constants.state.VALIDATED'))->get();

        // crear balance general
        // get numbers students that are in elearning
        $students_B = DB::table('solicitud_grupos_estudiante')
                            ->select('solicitud_grupos_estudiante.username as estudiante')
                            ->distinct()
                            ->join('grupos', 'grupos.id', '=', 'solicitud_grupos_estudiante.grupo_id')
                            ->join('solicituds', 'solicituds.id', '=', 'grupos.solicitud_id')
                            ->where([
                                ['solicituds.state', '=', '4'],
                                ['solicituds.periodo', '=', $current_period],
                                ['solicituds.modalidad', '=', 'B']
                            ])->get();

        $students_E = DB::table('solicitud_grupos_estudiante')
                            ->select('solicitud_grupos_estudiante.username as estudiante')
                            ->distinct()
                            ->join('grupos', 'grupos.id', '=', 'solicitud_grupos_estudiante.grupo_id')
                            ->join('solicituds', 'solicituds.id', '=', 'grupos.solicitud_id')
                            ->where([
                                ['solicituds.state', '=', '4'],
                                ['solicituds.periodo', '=', $current_period],
                                ['solicituds.modalidad', '=', 'E']
                            ])->get();

        $total_students_B_current = $students_B->count();
        $total_students_E_current = $students_E->count();

        //filtrar solicitudes elearning
        $requests_elearning = $requests->filter(function ($value, $key) {
          return $value->modalidad == 'E';
        });

        //filtrar solicitudes belearning
        $requests_blearning = $requests->filter(function ($value, $key) {
            return $value->modalidad == 'B';
        });

        $total_courses_elearning_current = $requests_elearning->count();
        $total_courses_blearning_current = $requests_blearning->count();

        $balance_general = array('e-learning' => array ('total_courses' => $total_courses_elearning_current, 'total_students' => $total_students_E_current),
                                 'b-learning' => array('total_courses' => $total_courses_blearning_current, 'total_students' => $total_students_B_current));

        // generate faculties arrays with totals
        $sw = false;
        $index_current=0;
        $faculties_analize = array();

        foreach($periods as $period) {
            // saltamos el periodo actual
            if ($sw == false) {
                $sw = true;
                $current_period = $period->nombre;
            }

            // consultar las solicitudes del periodo
            $requests = Solicitud::where('periodo', $period->nombre)->where('state', Config::get('constants.state.VALIDATED'))->get();

            // if the period hasn't request, skip it
            if ($requests->isEmpty()) {
                continue;
            }

            // get period's faculties in processing
            $faculties_period = DB::table('solicituds')->select('facultad')->distinct()->where('periodo', $period->nombre)->where('state', Config::get('constants.state.VALIDATED'))->get();

            // restart arrays totalize
            unset($total_courses_by_faculty_blearning);
            unset($total_courses_by_faculty_elearning);
            unset($total_students_by_faculty_blearning);
            unset($total_students_by_faculty_elearning);
           
            $total_students_B = 0;
            $total_students_E = 0;

            foreach ($faculties_period as $faculty) {
                $total_courses_by_faculty_blearning[$faculty->facultad] = 0;
                $total_courses_by_faculty_elearning[$faculty->facultad] = 0;
                $total_students_by_faculty_blearning[$faculty->facultad] = 0;
                $total_students_by_faculty_elearning[$faculty->facultad] = 0;

                //To build a faculties array
                if (array_search($faculty->facultad, $faculties_analize) === false) {
                    $faculties_analize[] = $faculty->facultad;
                }
            }

            // i iterate each faculty
            foreach($faculties_period as $faculty) {
                // I get everyone the students in blearning mode
                // get numbers students that are in elearning
                $students_B = DB::table('solicitud_grupos_estudiante')
                    ->select('solicitud_grupos_estudiante.username as estudiante')
                    ->distinct()
                    ->join('grupos', 'grupos.id', '=', 'solicitud_grupos_estudiante.grupo_id')
                    ->join('solicituds', 'solicituds.id', '=', 'grupos.solicitud_id')
                    ->where([
                        ['solicituds.state', '=', '4'],
                        ['solicituds.periodo', '=', $period->nombre],
                        ['solicituds.modalidad', '=', 'B'],
                        ['solicituds.facultad', '=', $faculty->facultad]
                    ])->get();

                $students_E = DB::table('solicitud_grupos_estudiante')
                    ->select('solicitud_grupos_estudiante.username as estudiante')
                    ->distinct()
                    ->join('grupos', 'grupos.id', '=', 'solicitud_grupos_estudiante.grupo_id')
                    ->join('solicituds', 'solicituds.id', '=', 'grupos.solicitud_id')
                    ->where([
                        ['solicituds.state', '=', '4'],
                        ['solicituds.periodo', '=', $period->nombre],
                        ['solicituds.modalidad', '=', 'E'],
                        ['solicituds.facultad', '=', $faculty->facultad]
                    ])->get();


                $total_courses_B = Solicitud::where('periodo', $period->nombre)
                                        ->where('state', Config::get('constants.state.VALIDATED'))
                                        ->where('modalidad', 'B')
                                        ->where('facultad', $faculty->facultad)
                                        ->count();

                $total_courses_E = Solicitud::where('periodo', $period->nombre)
                                        ->where('state', Config::get('constants.state.VALIDATED'))
                                        ->where('modalidad', 'E')
                                        ->where('facultad', $faculty->facultad)
                                        ->count();


                // i totalize students b-elearning
                $total_students_B = $total_students_B + $students_B->count();
                $total_students_by_faculty_blearning[$faculty->facultad] = $students_B->count();
                // i totalize students e-elearning
                $total_students_E = $total_students_E + $students_E->count();
                $total_students_by_faculty_elearning[$faculty->facultad] = $students_E->count();

                // i totalize courses b-elearning
                $total_courses_by_faculty_blearning[$faculty->facultad] = $total_courses_B;
                $total_courses_by_faculty_elearning[$faculty->facultad] = $total_courses_E;
            }

            // i store periods data 
            $comparative_offer_by_faculty[] = array('period' => $period->nombre,
                                                'total_students_blearning' => $total_students_by_faculty_blearning,
                                                'total_students_elearning' => $total_students_by_faculty_elearning,
                                                'total_courses_blearning' => $total_courses_by_faculty_blearning,
                                                'total_courses_elearning' => $total_courses_by_faculty_elearning);

        }

        // return view('reportes.temporal', ['total' => $comparative_offer_by_faculty, 'faculties_analize' => $faculties_analize]);
        $sw = false;
        // create arrays with totals by period
        foreach($periods as $period) {
            // saltamos el periodo actual
            if ($sw == false) {
                $sw = true;
                $current_period = $period->nombre;
            }

            $total_courses_elearning = 0;
            $total_courses_blearning = 0;

            // consultar las solicitudes del periodo
            $requests = Solicitud::where('periodo', $period->nombre)->where('state', Config::get('constants.state.VALIDATED'))->get();

            // if the period hasn't request, skip it
            if ($requests->isEmpty()) {
                continue;
            }

            //filtrar solicitudes elearning
            $requests_elearning = $requests->filter(function ($value, $key) {
                return $value->modalidad == 'E';
            });
  
            //filtrar solicitudes belearning
            $requests_blearning = $requests->filter(function ($value, $key) {
                return $value->modalidad == 'B';
            });

            $total_courses_blearning = $requests_blearning->count();
            $total_courses_elearning = $requests_elearning->count();

            $students_B = DB::table('solicitud_grupos_estudiante')
                ->select('solicitud_grupos_estudiante.username as estudiante')
                ->distinct()
                ->join('grupos', 'grupos.id', '=', 'solicitud_grupos_estudiante.grupo_id')
                ->join('solicituds', 'solicituds.id', '=', 'grupos.solicitud_id')
                ->where([
                    ['solicituds.state', '=', '4'],
                    ['solicituds.periodo', '=', $period->nombre],
                    ['solicituds.modalidad', '=', 'B'],
                ])->get();

            $students_E = DB::table('solicitud_grupos_estudiante')
                ->select('solicitud_grupos_estudiante.username as estudiante')
                ->distinct()
                ->join('grupos', 'grupos.id', '=', 'solicitud_grupos_estudiante.grupo_id')
                ->join('solicituds', 'solicituds.id', '=', 'grupos.solicitud_id')
                ->where([
                    ['solicituds.state', '=', '4'],
                    ['solicituds.periodo', '=', $period->nombre],
                    ['solicituds.modalidad', '=', 'E'],
                ])->get();

            $total_students_B = $students_B->count();
            $total_students_E = $students_E->count();

            // cargamos la totalización en el array comparative_offer_by_mode
            $total_mode = array('e-learning' => array ('total_courses' => $total_courses_elearning, 'total_students' => $total_students_E),
            'b-learning' => array('total_courses' => $total_courses_blearning, 'total_students' => $total_students_B));

            // create array with totalizations and counts 
            $comparative_offer_by_mode[] = array('period' => $period->nombre,
                                                'total_mode' => $total_mode);

            $total_comparative_offer[] = array('period' => $period->nombre,
                                                'total_students_blearning' => $total_students_B,
                                                'total_students_elearning' => $total_students_E,
                                                'total_courses_blearning' => $total_courses_blearning,
                                                'total_courses_elearning' => $total_courses_elearning);

        }

        // return view('reportes.temporal', ['comparative' => $comparative_offer_by_mode, 'total' => $total_comparative_offer]);

        // **********************************
        // create elearning details for level
        // **********************************

        // counter sujects by level
        $total_subjects_by_level = 0;
        // iterate by faculties
        foreach ($faculties as $faculty) {
            // initialize arrays and counters
            unset($detail_departments);
            $detail_departments = array();
            $total_subjects_faculty = 0;
            foreach ($faculties_departments as $department) {
                // to skip whether it do not be owned to faculty iteration
                if ($department->facultad != $faculty->facultad) continue;
                // init arrays and counters
                unset($detail_subjects);
                $detail_subjects = array();
                $total_subjects = 0;
                // count subjects
                foreach ($faculties_departments_subjects as $subject) {
                    if ($subject->departamento == $department->departamento && $subject->facultad == $faculty->facultad ) {
                        $total_students = 0;
                        // search the requests that has the subject
                        $requests_subject = Solicitud::where('periodo', $current_period)
                                                        ->where('facultad', $faculty->facultad)
                                                        ->where('departamento', $department->departamento)
                                                        ->where('codigo_asignatura', $subject->codigo_asignatura)
                                                        ->where('modalidad', 'E')
                                                        ->where('state', Config::get('constants.state.VALIDATED'))
                                                        ->get();

                        if ($requests_subject->isEmpty()) {
                            continue;
                        } else {
                            // it totalize the students
                            foreach ($requests_subject as $request) {
                                foreach ($request->grupos as $group) {
                                    $students = $group->estudiantes;
                                    $total_students = $total_students + $students->count();
                                }
                            }
                            $detail_subjects[] = array('code' => $subject->codigo_asignatura, 'enrolled' => $total_students, 'retention' => 0);
                            $total_subjects++;
                        }
                    }
                } // iterate subjects
                // departments detail
                // if departament be owned by faculty
                if ($total_subjects !== 0) {
                    $detail_departments[] = array('name' => $department->departamento, 'total_subjects' => $total_subjects, 'subjects' => $detail_subjects);
                    // add total subjects to faculty
                    $total_subjects_faculty = $total_subjects_faculty + $total_subjects;
                }
            } // iterate departments
            // faculties detail
            // $detail_departments = 'none';
            if ($total_subjects_faculty !== 0) {
                $detail_faculties[] = array('name' => $faculty->facultad, 'total_subjects' => $total_subjects_faculty, 'departments' => $detail_departments);
                $total_subjects_by_level = $total_subjects_by_level + $total_subjects_faculty;
            }
        } // iterate faculties
        $detail_offers_elearning = array('0' => array('name' => 'pregrado', 'total_subjects' => $total_subjects_by_level, 'faculties' => $detail_faculties));
    
        // **********************************
        // create blearning details for level 
        // **********************************

        // reinit faculties details array
        unset($detail_faculties);
        $detail_faculties = array();

        // counter sujects by level
        $total_subjects_by_level = 0;

        // departments & faculties arrays - implementation of courses blearning
        $faculties_implementation_b = array();

        // array with departament and its courses
        $departments_courses = [];
        $courses = [];

        // iterate by faculties
        foreach ($faculties as $faculty) {
            // initialize arrays and counters
            unset($detail_departments);
            $detail_departments = array();
            $total_subjects_faculty = 0;
            $total_departments = 0;
            $total_courses_faculty_implementation = 0;

            // departments array - implementation of courses
            unset($departmaments_implementation);
            $departments_implementation = array();

            foreach ($faculties_departments as $department) {
                // to skip whether it do not be owned to faculty iteration
                if ($department->facultad != $faculty->facultad) continue;
                // init arrays and counters
                unset($detail_subjects);
                $detail_subjects = array();
                $total_subjects = 0;
                $total_students_by_department = 0;

                // save courses
                $courses = '';

                $requests_departments_belearning = Solicitud::where('periodo', $current_period)
                                                    ->where('facultad', $faculty->facultad)
                                                    ->where('departamento', $department->departamento)
                                                    ->where('modalidad', 'B')
                                                    ->where('state', Config::get('constants.state.VALIDATED'))
                                                    ->get(); 

                $departments_implementation[] = array('name' => $department->departamento, 'total_courses' => $requests_departments_belearning->count());
                $total_courses_faculty_implementation = $total_courses_faculty_implementation + $requests_departments_belearning->count();
                // count subjects
                foreach ($faculties_departments_subjects as $subject) {
                    // if subject belongs to department and faculty then process it
                    if ($subject->departamento == $department->departamento && $subject->facultad == $faculty->facultad ) {
                        $total_students = 0;
                        // search the requests that has the subject
                        $requests_subject = Solicitud::where('periodo', $current_period)
                                                        ->where('facultad', $faculty->facultad)
                                                        ->where('departamento', $department->departamento)
                                                        ->where('codigo_asignatura', $subject->codigo_asignatura)
                                                        ->where('modalidad', 'B')
                                                        ->where('state', Config::get('constants.state.VALIDATED'))
                                                        ->get();

                        if ($requests_subject->isEmpty()) {
                            continue;
                        } else {
                            // it totalize the students
                            foreach ($requests_subject as $request) {
                                foreach ($request->grupos as $group) {
                                    $students = $group->estudiantes;
                                    $total_students = $total_students + $students->count();
                                    // save courses
                                    $courses = $courses.' '.$request->codigo_asignatura.'-'.$group->grupo;
                                }
                            }
                            $detail_subjects[] = array('code' => $subject->codigo_asignatura, 'enrolled' => $total_students, 'retention' => 0);
                            $total_subjects++;
                            // totalize students by department
                            $total_students_by_department = $total_students_by_department + $total_students;
                        }
                    } // block if subject belong to department and faculty
                } // iterate subjects
                // departments detail
                // if departament be owned by faculty
                if ($total_subjects !== 0) {
                    $detail_departments[] = array('name' => $department->departamento, 'total_subjects' => $total_subjects, 'total_students' => $total_students_by_department ,'subjects' => $detail_subjects);
                    // add total subjects to faculty
                    $total_subjects_faculty = $total_subjects_faculty + $total_subjects;
                    $total_departments++;
                }
                // $total_departments++;
                // set courses array into departments courses
                $departments_courses[$department->departamento] = $courses;
            } // iterate departments
            
            // faculties detail
            // $detail_departments = 'none';
            if ($total_subjects_faculty !== 0) {
                $detail_faculties[] = array('name' => $faculty->facultad, 'total_subjects' => $total_subjects_faculty, 'total_departments' => $total_departments, 'departments' => $detail_departments);
                $total_subjects_by_level = $total_subjects_by_level + $total_subjects_faculty;
            }
            // add departaments arrays to departments and faculties arrays - implementation of courses
            $faculties_implementation_b[] = array('name' => $faculty->facultad, 'total_courses' => $total_courses_faculty_implementation, 'departments' => $departments_implementation);

        } // iterate faculties

        $detail_offers_blearning = array('0' => array('name' => 'pregrado', 'total_subjects' => $total_subjects_by_level, 'faculties' => $detail_faculties));

        // implementation arrays
        $faculties_implementation_b_json = json_encode($faculties_implementation_b);

        // totalize programs and ciclos

        // get URL webservice
        $domainws = config('swebsiga.domain_ws');
        $ws_academia = config('swebsiga.academia_ws');

        $client = new Client();

        // get programs
        try {
            $response_programas = $client->request('GET', $domainws.$ws_academia.'/api/programas/'.$current_period);
            $body_response = $response_programas->getBody();
            $body_response_array = json_decode($body_response, TRUE);
            $programas = $body_response_array["data"]["programas"];

            // totalize students elearning by program
            $total_students_e_by_programa = [];
            $total_students_e = 0;
            foreach($programas as $p) {
                $query = "SELECT DISTINCT solicitud_grupos_estudiante.username FROM solicituds, grupos, solicitud_grupos_estudiante WHERE solicituds.id = grupos.solicitud_id AND solicitud_grupos_estudiante.grupo_id = grupos.id AND solicituds.periodo = '".$current_period."' AND solicitud_grupos_estudiante.cod_prog = '".$p[0]."' AND solicituds.modalidad = 'E'";
                $students = DB::select($query);
                $total_students_e_by_programa[$p[0]] = count($students);
                $total_students_e += count($students);
            }
        
            // totalize students belearning by program
            $total_students_b_by_programa = [];
            $total_students_b = 0;
            foreach($programas as $p) {
                $query = "SELECT DISTINCT solicitud_grupos_estudiante.username FROM solicituds, grupos, solicitud_grupos_estudiante WHERE solicituds.id = grupos.solicitud_id AND solicitud_grupos_estudiante.grupo_id = grupos.id AND solicituds.periodo = '".$current_period."' AND solicitud_grupos_estudiante.cod_prog = '".$p[0]."' AND solicituds.modalidad = 'B'";
                $students = DB::select($query);
                $total_students_b_by_programa[$p[0]] = count($students);
                $total_students_b += count($students);
            }

            $programas_asoc = [];
            foreach($programas as $p) {
                $programas_asoc[$p[0]] = $p[1];
            }
            array_multisort($total_students_e_by_programa, SORT_DESC, SORT_NUMERIC);
            array_multisort($total_students_b_by_programa, SORT_DESC, SORT_NUMERIC);


            // totalize students by ciclos
            // searched ciclos en el período
            $query = "SELECT DISTINCT solicitud_grupos_estudiante.ciclo FROM solicituds, grupos, solicitud_grupos_estudiante WHERE solicituds.id = grupos.solicitud_id AND solicitud_grupos_estudiante.grupo_id = grupos.id AND solicituds.periodo = '".$current_period."'";
            $ciclos = DB::select($query);
            $total_students_by_ciclo = [];
            $total_students_ciclo = 0;
            foreach($ciclos as $c) {
                $query = "SELECT DISTINCT solicitud_grupos_estudiante.username FROM solicituds, grupos, solicitud_grupos_estudiante WHERE solicituds.id = grupos.solicitud_id AND solicitud_grupos_estudiante.grupo_id = grupos.id AND solicituds.periodo = '".$current_period."' AND solicitud_grupos_estudiante.ciclo = '".$c->ciclo."'";
                $students = DB::select($query);
                $total_students_by_ciclo[$c->ciclo] = count($students);
                $total_students_ciclo += count($students);
            }
            array_multisort($total_students_by_ciclo, SORT_DESC, SORT_NUMERIC);
        } catch (ClientException $e) {
            if ($response->getStatusCode() == 404) {
                $record_log = ' # '.$s->id.' no larqahay registros'.$body_php['data']['message'].PHP_EOL;
            } else {
                $record_log = ' # '.$s->id.' error'.$str_body.PHP_EOL;
            }
            // $this->saveLog($fp, $record_log);
        }

        // $strdetail = var_dump($detail_offers_blearning); 
        // $strdetail = var_dump($faculties_departments_subjects);
        // $strdetail = var_dump($faculties_departments);
        // $strdetail = var_dump($departments_courses);
        // return ($strdetail);

        return view('reportes.general', ['balance_general' => $balance_general,
                    'periods' => $periods,
                    'comparative_offer_by_mode' => $comparative_offer_by_mode,
                    'comparative_offer_by_faculty' => $comparative_offer_by_faculty,
                    'current_period' => $current_period,
                    'faculties' => $faculties,
                    'faculties_departments' => $faculties_departments,
                    'faculties_departments_subjects' => $faculties_departments_subjects,
                    'total_subjects_by_department' => $total_subjects_by_department,
                    'total_subjects_by_faculty' => $total_subjects_by_faculty,
                    'detail_offers_elearning' => $detail_offers_elearning,
                    'detail_offers_blearning' => $detail_offers_blearning,
                    'total_comparative_offer' => $total_comparative_offer,
                    'faculties_implementation' => $faculties_implementation_b_json,
                    'faculties_analize' => $faculties_analize,
                    'programas_asoc' => $programas_asoc, 'programas' => $programas,
                    'total_students_e_by_programa' => $total_students_e_by_programa,
                    'total_students_e' => $total_students_e,
                    'total_students_b_by_programa' => $total_students_b_by_programa,
                    'total_students_b' => $total_students_b,
                    'total_students_by_ciclo' => $total_students_by_ciclo,
                    'total_students_ciclo' => $total_students_ciclo,
                    'departments_courses' => $departments_courses
                    ]);
    

    }
}
