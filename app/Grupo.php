<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grupo extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function solicitud()
    {
        return $this->belongsTo('App\Solicitud');
    }

    public function estudiantes()
    {
        return $this->hasMany('App\solicitud_grupo_estudiante');
    }
}
