<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Solicitud;
use App\Grupo;
use App\Parametro;
use App\solicitud_grupo_estudiante;
use App\Cancelacion;

use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException;

class ProcessUpdateEnroll implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        date_default_timezone_set('America/Bogota');
        // dominio servicio web
        $domainws = config('swebsiga.domain_ws');
        $ws_academia = config('swebsiga.academia_ws');
        $ws_virtual = config('swebsiga.virtual_ws');
        
        // abrir logs
        $fp = fopen("./log/swebsiga-update.log", "a+");
        $record_log = ' ************BEGIN UPDATE**************'.PHP_EOL;
        $this->saveLog($fp, $record_log);

        $swon = false;
        /* cargar parametros:
            PERIODO_ACTUAL, INICIO_MATRICULA, FIN_MATRICULA, CATEGORY_ID
        */
        $parametro = Parametro::where('nombre','periodo-actual')->first();
        if (!$parametro) {
            $record_log = ' parametro periodo-actual no encontrado'.PHP_EOL;
            $this->saveLog($fp, $record_log);
        } else {
            $PERIODO_ACTUAL = $parametro->valor;
            $record_log = ' parametro periodo-actual '.$PERIODO_ACTUAL.PHP_EOL;
            $this->saveLog($fp, $record_log);
        }

        $parametro = Parametro::where('nombre','inicio-matricula')->first();
        if (!$parametro) {
            $record_log = ' parametro inicio-matricula no encontrado'.PHP_EOL;
        } else {
            $INICIO_MATRICULA = $parametro->valor;
            $record_log = ' parametro inicio-matricula '.$INICIO_MATRICULA.PHP_EOL;
            $this->saveLog($fp, $record_log);
        }

        $parametro = Parametro::where('nombre','fin-matricula')->first();
        if (!$parametro) {
            $record_log = ' parametro fin-matricula no encontrado'.PHP_EOL;
        } else {
            $FIN_MATRICULA = $parametro->valor;
            $record_log = ' parametro fin-matricula '.$FIN_MATRICULA.PHP_EOL;
            $this->saveLog($fp, $record_log);
        }

        $parametro = Parametro::where('nombre','id-category')->first();
        if (!$parametro) {
            $record_log = ' parametro id-category no encontrado'.PHP_EOL;
        } else {
            $CATEGORY_ID = $parametro->valor;
            $record_log = ' parametro id-category '.$CATEGORY_ID.PHP_EOL;
            $this->saveLog($fp, $record_log);
        }
        // validamos la parametrizacion 
        if (isset($PERIODO_ACTUAL) && isset($INICIO_MATRICULA) && isset($FIN_MATRICULA) && isset($CATEGORY_ID)) {
            // cambiamos estado de las solicitudes de validado a update
            // Solicitud::where('state', 4)->where('periodo', $PERIODO_ACTUAL)->update(['state' => 7]);
            // obtenemos las solicitudes OK para actualizarlas
            $solicitudes = Solicitud::where('periodo', $PERIODO_ACTUAL)->where('state', '7')->get();
            $client = new \GuzzleHttp\Client();
            $record_log = ' #  BEGIN '.PHP_EOL;
            $this->saveLog($fp, $record_log);
            // recorremos las solicitudes
            foreach($solicitudes as $s) {
                // if ($s->state != '4') continue;
                $s->state = '3';
                $s->save();
                // formamos el shortname ej: 561231-aaragon-E
                $shortname_docente = $s->codigo_asignatura.'-'.$s->username.'-'.$s->modalidad;
                $record_log = ' # '.$s->id.' matricular grupos en '.$shortname_docente.PHP_EOL;
                $this->saveLog($fp, $record_log);
                //buscar curso para obtener el id de curso y crear grupos
                $curso_no_encontrado = false;
                try {
                    $response_curso = $client->request('GET', $domainws.$ws_virtual.'/api/curso/'.$shortname_docente);
                } catch (ClientException $e) {
                    // $status_cod_error = Psr7\str($e->getResponse());
                    // docente no tiene curso en siga
                    // no debería entrar aqui sería extraño
                    $response = $e->getResponse();
                    $body = $response->getBody();
                    $str_body = (string) $body;
                    $body_php = json_decode($str_body, TRUE);
                    $record_log = ' # '.$s->id.' curso no encontrado extraño'.$str_body.PHP_EOL;
                    $this->saveLog($fp, $record_log);
                    $curso_no_encontrado = true;
                    // saltamos la solicitud
                    continue;
                }
                $status_code_curso = $response_curso->getStatusCode();
                $body_curso = $response_curso->getBody();
                $body_curso_array = json_decode($body_curso, TRUE);
                // obtenemos courseid
                $courseid = $body_curso_array["data"]["id"];
                // obtenemos los grupos del curso en SIGA para determinar si cada grupo ha sido creado y evitar q falle
                $failed_get_grupos = false;
                try {
                    $response_grupos = $client->request('GET', $domainws.$ws_virtual.'/api/curso/grupos/'.$courseid);
                } catch (ClientException $e) {
                    $response = $e->getResponse();
                    $body = $response->getBody();
                    $str_body = (string) $body;
                    $body_php = json_decode($str_body, TRUE);
                    $failed_get_grupos = true;
                    $record_log = ' # '.$s->id.' fallo get grupos '.$str_body.PHP_EOL;
                    $this->saveLog($fp, $record_log);
                    continue;
                }
                $body_grupos = $response_grupos->getBody();
                $body_grupos_array = json_decode($body_grupos, TRUE);
                // obtenemos los grupos del curso
                $grupos_curso = $body_grupos_array["data"]["groups"];
                // obtenemos los grupos de la solicitud
                $grupos = $s->grupos;
                // iteramos por los grupos de la solicitud
                foreach($grupos as $g) {
                    // si el curso no tiene grupos o no existe se crea para evitar una excepcion
                    $grupoExiste = false;
                    // totalizamos cuantos grupos tiene el curso
                    $total_grupos = count($grupos_curso);
                    if ($total_grupos > 0) {
                        // ya tiene grupos el curso en campus entonces buscamos si el grupo de la solicitud ya existe
                        foreach ($grupos_curso as $grupo_curso) {
                            // $record_log = ' # '.$s->id.' comparo '.$grupo_curso["name"].' con '.$g->grupo.' curso '.$shortname_docente.PHP_EOL;
                            // $this->saveLog($fp, $record_log);
                            if ($grupo_curso["name"] == $g->grupo) {
                                // guardamos el id group
                                $groupid = $grupo_curso["id"];
                                $groupname = $grupo_curso["name"];
                                $grupoExiste = true;
                                // $record_log = ' # '.$s->id.' grupo '.$g->grupo.' ya existe en '.$shortname_docente.PHP_EOL;
                                // $this->saveLog($fp, $record_log);
                                break;
                            }
                        }
                    }

                    if (!$grupoExiste) {
                        // creamos el grupos ya que no existe 
                        $gruponew = array("courseid" => $body_curso_array["data"]["id"],
                                            "name" => $g->grupo,
                                            "description" => $g->grupo);
                        
                        try {
                            $response_grupo = $client->request('POST', $domainws.$ws_virtual.'/api/grupo',
                                [ 'json' => $gruponew]);
                        } catch (ClientException $e) {
                            $response = $e->getResponse();
                            $body = $response->getBody();
                            $str_body = (string) $body;
                            $body_php = json_decode($str_body, TRUE);
                            
                            $record_log = ' # '.$s->id.' fallo creando grupo salta'.$str_body.PHP_EOL;
                            $this->saveLog($fp, $record_log);
                            // abortamos el procesamiento de este grupo
                            continue;
                        }
                        // guardamos el id grupo recien creado
                        $body_group = $response_grupo->getBody();
                        $body_group_array = json_decode($body_group, TRUE);
                        $groupid =  $body_group_array['data']['id'];
                        $groupname = $body_group_array['data']['name'];

                        $record_log = ' # '.$s->id.' grupo '.$g->grupo.' creado en '.$shortname_docente.PHP_EOL;
                        $this->saveLog($fp, $record_log);
                    } // conditional create groups

                    //obtenemos los estudiante del grupo para matricular de academia
                    try {
                        $response_estudiantes_grupo = $client->request('GET', $domainws.$ws_academia.'/api/estudiantes/'.$s->codigo_asignatura.'-'.$g->grupo.'/'.$PERIODO_ACTUAL);
                    } catch (ClientException $e) {
                        $response = $e->getResponse();
                        $body = $response->getBody();
                        $str_body = (string) $body;
                        $body_php = json_decode($str_body, TRUE);

                        $record_log = ' # '.$s->id.' falla get students group: '.$g->grupo.$str_body.PHP_EOL;
                        $this->saveLog($fp, $record_log);
                        // abortamos la matriculacion de este grupo
                        continue;
                    }

                    $body_estudiantes_grupo = $response_estudiantes_grupo->getBody();
                    $body_estudiantes_grupo_array = json_decode($body_estudiantes_grupo, TRUE);

                    //obtenemos estudiantes matriculados
                    try {
                        $response_students_enrolled = $client->request('GET', $domainws.$ws_virtual.'/api/curso/estudiantes/'.$courseid);
                    } catch(ClientException $e) {
                        $response = $e->getResponse();
                        $body = $response->getBody();
                        $str_body = (string) $body;
                        $body_exception = json_decode($body, TRUE);

                        if ($body_exception['data']['status'] == "not-found") {
                            $record_log = ' # '.$s->id.' no tiene estudiantes '.PHP_EOL;
                            $this->saveLog($fp, $record_log);
                        }
                    }

                    // matriculamos los estudiantes que hacen falta
                    for ($i = 0; $i < count($body_estudiantes_grupo_array['data']['estudiantes']); $i++) {
                        // determinar si el estudiante esta matriculado
                        $username_estudiante = strtolower($body_estudiantes_grupo_array['data']['estudiantes'][$i][0]);
                        
                        if ( !stristr($username_estudiante, '?') === false ) continue; 
                        
                        $state_user = '';
                        $entro = false;
                        try {
                            $response_matricula = $client->request('GET', $domainws.$ws_virtual.'/api/matricula/'.$username_estudiante.'/'.$shortname_docente);
                        } catch (ClientException $e) {
                            $response = $e->getResponse();
                            $body = $response->getBody();
                            $str_body = (string) $body;
                            $body_exception = json_decode($body, TRUE);

                            $entro = true;  

                            if ($body_exception['data']['status'] == "USER_NOT_FOUND") {
                                $record_log = ' # '.$s->id.' estudiante no existe '.PHP_EOL;
                                $state_user = "USER_NOT_FOUND";
                                $this->saveLog($fp, $record_log);
                            }

                            if ($body_exception['data']['status'] == "COURSE_NOT_FOUND") {
                                $record_log = ' # '.$s->id.' courso no existe extraño'.PHP_EOL;
                                $this->saveLog($fp, $record_log);
                                continue;
                            }

                            if ($body_exception['data']['status'] == "USER_NOT_ENROLL") {
                                $record_log = ' # '.$s->id.' estudiante no matriculado '.PHP_EOL;
                                $state_user = "USER_NOT_ENROLL";
                                $this->saveLog($fp, $record_log);
                            }
                            $entro = true;
                        }

                        if (!$entro) {
                            $body_matricula = $response_matricula->getBody();
                            $body_matricula = json_decode($body_matricula, TRUE);
                            // en update será true esta validación muchos registros
                            if ($body_matricula['data']['status'] == 'enrolled') {
                                // $record_log = ' # '.$s->id.' '.$username_estudiante.' matriculado en '.$shortname_docente.' grupo '.$groupname.PHP_EOL;
                                // $this->saveLog($fp, $record_log);
                                continue;
                            }
                        }   
                        
                        if ($state_user == "USER_NOT_FOUND") {
                            //creamos el usuario
                            $usuario = array("username" => $username_estudiante,
                                            "password" => "Uao.2018",
                                            "nombre" => $username_estudiante,
                                            "apellido" => $username_estudiante,
                                            "email" => $username_estudiante.'@uao.edu.co',
                                            "auth" => "ldap");
                            
                            $create_user = true;

                            try {
                                $response_create_user = $client->request('POST', $domainws.$ws_virtual.'/api/usuario',
                                                    [ 'json' => $usuario]);
                            } catch (ClientException $e) {
                                $response = $e->getResponse();
                                $body = $response->getBody();
                                $str_body = (string) $body;
                                $body_php = json_decode($str_body, TRUE);

                                $record_log = ' # '.$s->id.' fallo creacion usuario '.$username_estudiante.PHP_EOL;
                                $this->saveLog($fp, $record_log);
                                $create_user = false;
                                // si falla saltamos al proximo estudiante
                                continue;
                            }
                            // obtenemos el id user
                            $body_usuario = $response_create_user->getBody();
                            $body_usuario_array = json_decode($body_usuario, TRUE);

                            $userid = $body_usuario_array['data']['id'];

                            $record_log = ' # '.$s->id.' ok creacion user '.$username_estudiante.PHP_EOL;
                            $this->saveLog($fp, $record_log);                        
                        } else {
                            // usuario ya existe en campus
                            try {
                                $response_get_usuario = $client->request('GET', $domainws.$ws_virtual.'/api/usuario/'.$username_estudiante);
                            } catch (ClientException $e) {
                                $response = $e->getResponse();
                                $body = $response->getBody();
                                $str_body = (string) $body;
                                $body_php = json_decode($str_body, TRUE);
                                // usuario no existe lo creamos
                                $record_log = ' # '.$s->id.' fallo get usuario extraño'.$username_estudiante.PHP_EOL;
                                $this->saveLog($fp, $record_log);
                                continue;
                            }
                            // obtenemos el id user
                            $body_usuario = $response_get_usuario->getBody();
                            $body_usuario_array = json_decode($body_usuario, TRUE);

                            $userid = $body_usuario_array['data']['id'];
                        }
                        // matricular usuario
                        $estudiante = array("shortname" => $shortname_docente,
                                            "username" => $username_estudiante,
                                            "role" => "student",
                                            "timestart" => $INICIO_MATRICULA,
                                            "timeend" => $FIN_MATRICULA);
                        $failed = false;

                        try {                   
                            $response_enrol = $client->request('POST', $domainws.$ws_virtual.'/api/matricula',
                                            [ 'json' => $estudiante]);
                        } catch(ClientException $e) {
                            $response = $e->getResponse();
                            $body = $response->getBody();
                            $str_body = (string) $body;
                            $body_php = json_decode($str_body, TRUE);
                            // here
                            $failed = true;
                            $record_log = ' # '.$s->id.' fallo matriculacion estudiante '.$username_estudiante.PHP_EOL;
                            $this->saveLog($fp, $record_log);
                            // no tiene sentido seguir
                            continue;
                        }
                        // asignar a grupo
                        $record_log = ' # '.$s->id.' estudiante '.$username_estudiante.' matriculado en '.$shortname_docente.PHP_EOL;
                        $this->saveLog($fp, $record_log);

                        // si tiene cancelación se retira

                        $deleteRows = Cancelacion::where('username', $username_estudiante)->where('codigo_asignatura', $s->codigo_asignatura)->delete();

                        if ($deleteRows != 0) {
                            $record_log = ' # '.$s->id.' '.$username_estudiante.' retirado cancelacion '.$shortname_docente.PHP_EOL;
                            $this->saveLog($fp, $record_log);
                        }

                        // guardamos estudiante en solicitud-grupos-estudiantes
                        $sge = new solicitud_grupo_estudiante;
                        $sge->grupo_id = $g->id;
                        $sge->username = strtolower($body_estudiantes_grupo_array['data']['estudiantes'][$i][0]);
                        $sge->cod_prog = $body_estudiantes_grupo_array['data']['estudiantes'][$i][1];
                        $sge->ciclo = $body_estudiantes_grupo_array['data']['estudiantes'][$i][2];
                        $sge->save();

                        // agregamos el estudiante al grupo
                        $failed_grouping = false;
                        $member = array("groupid" => $groupid, "userid" => $userid);
                        try {
                            $response_addmember = $client->request('POST', $domainws.$ws_virtual.'/api/grupo/add',
                                ['json' => $member]);
                        } catch (ClientException $e){
                            $response = $e->getResponse();
                            $body = $response->getBody();
                            $str_body = (string) $body;
                            $body_php = json_decode($str_body, TRUE);
                            
                            $failed_grouping = true;
                            $record_log = ' # '.$s->id.' fallo add estudiante '.strtolower($body_estudiantes_grupo_array['data']['estudiantes'][$i][0]).' a grupo '.$groupid.PHP_EOL;
                            $this->saveLog($fp, $record_log);
                        }
                        if (!$failed_grouping) {
                            $record_log = ' # '.$s->id.' estudiante '.$body_estudiantes_grupo_array['data']['estudiantes'][$i][0].' agregado al grupo '.$groupid.PHP_EOL;
                            $this->saveLog($fp, $record_log);
                        }
                    } // loop students
                } // loop grupos
                $record_log = ' # '.$s->id.' finished update '.PHP_EOL;
                $this->saveLog($fp, $record_log);
                $s->state = '4';
                $time_unix_current = time();
                $s->fecha_procesamiento = date("Y-m-d H:i:s", $time_unix_current);
                $s->save();
            } // loop solicitudes
        } // endif valida parametros
        $record_log = ' ************ending process update**************'.PHP_EOL;
        $this->saveLog($fp, $record_log);
        fclose($fp);
    } // process

    public function saveLog($fp, $record_log) {
        $tiempo = date('d-m-Y H:i:s');
        $record_log = $tiempo.$record_log;
        fwrite($fp, $record_log);
        return;
    }
}
