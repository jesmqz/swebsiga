<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Solicitud;
use App\Grupo;
use App\Parametro;
use App\solicitud_grupo_estudiante;

use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException;

use App\Mail\SolicitudProcesada;
use Illuminate\Support\Facades\Mail;

class ProcessSolicitud implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        date_default_timezone_set('America/Bogota');
        // dominio servicio web
        $domainws = config('swebsiga.domain_ws');
        $ws_academia = config('swebsiga.academia_ws');
        $ws_virtual = config('swebsiga.virtual_ws');
        $campus_url = config('swebsiga.campus_url');

        // abrir logs
        $fp = fopen("./log/swebsiga.log", "a+");
        $record_log = ' ************inicio**************'.PHP_EOL;
        $this->saveLog($fp, $record_log);

        $swon = false;
        /* cargar parametros:
            PERIODO_ACTUAL, INICIO_MATRICULA, FIN_MATRICULA, CATEGORY_ID
        */
        $parametro = Parametro::where('nombre','periodo-actual')->first();
        if (!$parametro) {
            $record_log = ' parametro periodo-actual no encontrado'.PHP_EOL;
            $this->saveLog($fp, $record_log);
        } else {
            $PERIODO_ACTUAL = $parametro->valor;
            $record_log = ' parametro periodo-actual '.$PERIODO_ACTUAL.PHP_EOL;
            $this->saveLog($fp, $record_log);
        }

        $parametro = Parametro::where('nombre','inicio-matricula')->first();
        if (!$parametro) {
            $record_log = ' parametro inicio-matricula no encontrado'.PHP_EOL;
        } else {
            $INICIO_MATRICULA = $parametro->valor;
            $record_log = ' parametro inicio-matricula '.$INICIO_MATRICULA.PHP_EOL;
            $this->saveLog($fp, $record_log);
        }

        $parametro = Parametro::where('nombre','fin-matricula')->first();
        if (!$parametro) {
            $record_log = ' parametro fin-matricula no encontrado'.PHP_EOL;
        } else {
            $FIN_MATRICULA = $parametro->valor;
            $record_log = ' parametro fin-matricula '.$FIN_MATRICULA.PHP_EOL;
            $this->saveLog($fp, $record_log);
        }

        $parametro = Parametro::where('nombre','id-category')->first();
        if (!$parametro) {
            $record_log = ' parametro id-category no encontrado'.PHP_EOL;
        } else {
            $CATEGORY_ID = $parametro->valor;
            $record_log = ' parametro id-category '.$CATEGORY_ID.PHP_EOL;
            $this->saveLog($fp, $record_log);
        }

        // validamos la parametrizacion 
        if (isset($PERIODO_ACTUAL) && isset($INICIO_MATRICULA) && isset($FIN_MATRICULA) && isset($CATEGORY_ID)) {
            $solicitudes = Solicitud::where('periodo', $PERIODO_ACTUAL)->where('state', 0)->get();
            $client = new \GuzzleHttp\Client();
            $record_log = ' #  BEGIN '.PHP_EOL;
            $this->saveLog($fp, $record_log);
            // recorremos las solicitudes
            foreach($solicitudes as $s) {
                // solicitud pausada
                if ($s->state == '3') continue;
                if ($s->state == '4') continue;
                if ($s->state == '5') continue;
                if ($s->state == '6') continue;
                // crea el shortname docente CODIGO_ASIGNATURA-USERNAME-MODALIDAD
                $shortname_docente = $s->codigo_asignatura.'-'.$s->username.'-'.$s->modalidad;
                // PROCESO RECONOCER DOCENTE
                if ($s->state == '0') {
                    $docente_no_encontrado_academia = false;
                    // buscamos en academia
                    try {
                        $response_docente = $client->request('GET', $domainws.$ws_academia.'/api/docente/'.$s->username);
                    } catch (ClientException $e) {
                        $response = $e->getResponse();
                        $body = $response->getBody();
                        $str_body = (string) $body;
                        $body_php = json_decode($str_body, TRUE);

                        $docente_no_encontrado_academia = true;

                        if ($response->getStatusCode() == 404) {
                            $record_log = ' # '.$s->id.' docente no encontrado academia'.$body_php['data']['message'].PHP_EOL;
                        } else {
                            $record_log = ' # '.$s->id.' docente no encontrado academia'.$str_body.PHP_EOL;
                        }
                        $this->saveLog($fp, $record_log);
                    }
                    // si docente fue encontrado en academia formar array docente
                    if (!$docente_no_encontrado_academia) {
                        $body_docente = $response_docente->getBody();
                        $body_docente_array = json_decode($body_docente, TRUE);
                        $docente_low = strtolower($body_docente_array["data"]["firstname"]);
                        $docente_firstname = ucwords($docente_low);
                        $docente_low = strtolower($body_docente_array["data"]["lastname"]);
                        $docente_lastname = ucwords($docente_low);
                        $docente = array("firstname" => $docente_firstname, "lastname" => $docente_lastname);

                        $record_log = ' # '.$s->id.' docente encontrado academia '.$s->username.PHP_EOL;
                        $this->saveLog($fp, $record_log);
                    }

                    // crear usuario
                    // verificar si existe
                    $docente_no_encontrado_siga = false;
                    try {
                        $response_docente = $client->request('GET', $domainws.$ws_virtual.'/api/usuario/'.$s->username);
                    } catch (ClientException $e) {
                        $response = $e->getResponse();
                        $body = $response->getBody();
                        $str_body = (string) $body;
                        $body_php = json_decode($str_body, TRUE);

                        $docente_no_encontrado_siga = true;

                        if ($response->getStatusCode() == 404) {
                            $record_log = ' # '.$s->id.' docente no encontrado en siga '.$body_php['data']['message'].PHP_EOL;
                            $this->saveLog($fp, $record_log);
                        } else {
                            $record_log = ' # '.$s->id.' docente no encontrado en siga '.$str_body.PHP_EOL;
                            $this->saveLog($fp, $record_log);
                        }
                    }

                    // si docente fue encontrado en siga formar arreglo docente de lo contrario creamos el usuario en siga
                    if (!$docente_no_encontrado_siga) {
                        // docente fue encontrado en siga, formamos el array docente 
                        $body_docente = $response_docente->getBody();
                        $body_docente_array = json_decode($body_docente, TRUE);
                        $docente = array("firstname" => $body_docente_array["data"]["firstname"], "lastname" => $body_docente_array["data"]["lastname"]);
                        // cambiar estado solicitud a: usuario creado
                        $s->state = '1';
                        $s->save();
                    } else {
                        // proceso crear usuario en moodle
                        // si docente esta en academia asignamos el firstname y lastname
                        if (!$docente_no_encontrado_academia) {
                            $usuario = array("username" => $s->username,
                                            "password" => "Uao.2018",
                                            "nombre" => $docente["firstname"],
                                            "apellido" => $docente["lastname"],
                                            "email" => $s->username.'@uao.edu.co',
                                            "auth" => "ldap");
                        } else {
                                $usuario = array("username" => $s->username,
                                            "password" => "Uao.2018",
                                            "nombre" => $s->username,
                                            "apellido" => $s->username,
                                            "email" => $s->username.'@uao.edu.co',
                                            "auth" => "ldap");

                                $docente = array("firstname" => $s->username, "lastname" => $s->username);
                        }
                   
                        try {
                            $response_create_user = $client->request('POST', $domainws.$ws_virtual.'/api/usuario',
                                                [ 'json' => $usuario]);
                        } catch (ClientException $e) {
                            $response = $e->getResponse();
                            $body = $response->getBody();
                            $str_body = (string) $body;
                            $body_php = json_decode($str_body, TRUE);

                            $record_log = ' # '.$s->id.' fallo creacion usuario docente en siga con '.$s->username.PHP_EOL;
                            $this->saveLog($fp, $record_log);
                            $create_user = false;

                            $record_log = ' # '.$s->id.' saltamos solicitud '.$s->username.PHP_EOL;
                            $this->saveLog($fp, $record_log);
                            // saltamos a la proxima solicitud
                            continue;
                        }

                        $s->state = '1';
                        $s->save();
                        $record_log = ' # '.$s->id.' ok creacion usuario '.$s->username.PHP_EOL;
                        $this->saveLog($fp, $record_log);
                    }  // proceso crear usuario
                } // state igual a 0

                // PROCESO CREAR ESPACIO
                if ($s->state == '1') {
                    $asignatura_no_encontrada = false;
                    // obtener datos de la asignatura
                    try {
                        $response_asignatura = $client->request('GET', $domainws.$ws_academia.'/api/asignatura/'.$s->codigo_asignatura);
                        
                        $status_code_asignatura = $response_asignatura->getStatusCode();
                        $body_asignatura = $response_asignatura->getBody();
                        $body_asignatura_array = json_decode($body_asignatura, TRUE);
                        $nombre_low = strtolower($body_asignatura_array["data"]["nombre"]);
                        $nombre_asignatura_title = ucwords($nombre_low);
                        $asignatura = array("nombre" => $nombre_asignatura_title);
                    } catch (ClientException $e) {
                        $response = $e->getResponse();
                        $body = $response->getBody();
                        $str_body = (string) $body;
                        $body_php = json_decode($str_body, TRUE);
    
                        $asignatura_no_encontrada = true;
    
                        $record_log = ' # '.$s->id.' no existe asignatura extraño '.$s->username.PHP_EOL;
                        $this->saveLog($fp, $record_log);
                        // saltamos a la proxima solicitud
                        continue;
                    }
    
                    // buscamos el curso
                    $curso_no_encontrado= false;
                    try {
                        $response_curso = $client->request('GET', $domainws.$ws_virtual.'/api/curso/'.$shortname_docente);
                    } catch (ClientException $e) {
                        // $status_cod_error = Psr7\str($e->getResponse());
                        // docente no tiene curso en siga
                        $response = $e->getResponse();
                        $body = $response->getBody();
                        $str_body = (string) $body;
                        $body_php = json_decode($str_body, TRUE);

                        if ($response->getStatusCode() == 404) {
                            $record_log = ' # '.$s->id.' curso no encontrado '.$body_php['data']['message'].PHP_EOL;
                            $this->saveLog($fp, $record_log);
                            $curso_no_encontrado = true;
                        } else {
                            $record_log = ' # '.$s->id.' curso no encontrado '.$str_body.PHP_EOL;
                            $this->saveLog($fp, $record_log);
                            $curso_no_encontrado = true;
                        }
                    }

                    if ($curso_no_encontrado) {
                        //docente no tiene curso ... entonces se busca el semilla
                        $curso_semilla_no_encontrado = false;
                        // creamos el shortname del curso semilla CODIGO_ASIGNATURA-MODALIDAD-*
                        $shortname_semilla = $s->codigo_asignatura.'-'.$s->modalidad.'*';

                        try {
                            $response_cursosemilla = $client->request('GET', $domainws.$ws_virtual.'/api/curso/'.$shortname_semilla);
                        } catch (ClientException $e) {
                            // $status_cod_error = Psr7\str($e->getResponse());
                            // no existe curso semilla
                            $response = $e->getResponse();
                            $body = $response->getBody();
                            $str_body = (string) $body;
                            $body_php = json_decode($str_body, TRUE);

                            if ($response->getStatusCode() == 404) {
                                $record_log = ' # '.$s->id.' curso semilla no encontrado '.$body_php['data']['message'].PHP_EOL;
                                $this->saveLog($fp, $record_log);
                                $curso_semilla_no_encontrado = true;
                            } else {
                                $record_log = ' # '.$s->id.' curso semilla no encontrado '.$str_body.PHP_EOL;
                                $this->saveLog($fp, $record_log);
                                $curso_semilla_no_encontrado = true;
                            }
                        }

                        if ($curso_semilla_no_encontrado) {
                            // crear un curso limpio
                            $record_log = ' # '.$s->id.' crear curso '.$shortname_docente.PHP_EOL;
                            $this->saveLog($fp, $record_log);

                            $newcurso = array ("fullname" => $asignatura['nombre'].'-'.$docente["firstname"].' '.$docente["lastname"],
                                                "shortname" => $shortname_docente,
                                                "categoryid" => $CATEGORY_ID);
                            $failed = false;
                            try {
                                $response_crear_curso = $client->request('POST', $domainws.$ws_virtual.'/api/curso',
                                    [ 'json' => $newcurso]);
                            } catch (ClientException $e) {
                                $response = $e->getResponse();
                                $body = $response->getBody();
                                $str_body = (string) $body;
                                $body_php = json_decode($str_body, TRUE);

                                $record_log = ' # '.$s->id.' falla crear curso nuevo '.$str_body.PHP_EOL;
                                $this->saveLog($fp, $record_log);
                                $failed = true;
                            }

                            if (!$failed) {
                                // curso fue creado
                                $status_code_curso = $response_crear_curso->getStatusCode();
                                $body_crear_curso = $response_crear_curso->getBody();
                                $body_crear_curso_array = json_decode($body_crear_curso, TRUE);

                                $record_log = ' # '.$s->id.' success curso nuevo creado '.$newcurso["shortname"].PHP_EOL;
                                $this->saveLog($fp, $record_log);
                                
                                // cambiar estado de solicitud a espacio creado
                                $s->state = '2';
                                $s->save();

                                // matricular docente
                                $matricula_docente = array("shortname" => $shortname_docente,
                                                            "username" => $s->username,
                                                            "role" => "teacher",
                                                            "timestart" => $INICIO_MATRICULA,
                                                            "timeend" => $FIN_MATRICULA);
                                $failed = false;
                                try {
                                    $response_matricula_docente = $client->request('POST', $domainws.$ws_virtual.'/api/matricula',
                                        [ 'json' => $matricula_docente]);
                                } catch (ClientException $e) {
                                    $response = $e->getResponse();
                                    $body = $response->getBody();
                                    $str_body = (string) $body;
                                    $body_php = json_decode($str_body, TRUE);
        
                                    $record_log = ' # '.$s->id.' falla enroll docente '.$str_body.PHP_EOL;
                                    $this->saveLog($fp, $record_log);

                                    $failed = true;
                                }

                                $record_log = ' # '.$s->id.' docente matriculado '.$newcurso["shortname"].PHP_EOL;
                                $this->saveLog($fp, $record_log);
                                
                                // cambiar estado de la solicitud
                                // CAMBIO
                                $s->state = '3';
                                $s->save();
                            }
                        } else {
                            // DUPLICAR CURSO DESDE EL SEMILLA
                            $record_log = ' # '.$s->id.' duplicar curso '.$shortname_docente.PHP_EOL;
                            $this->saveLog($fp, $record_log);

                            $body_cursosemilla = $response_cursosemilla->getBody();
                            $body_cursosemilla_array = json_decode($body_cursosemilla, TRUE);

                            $newcurso = array ("oldcourseid" => $body_cursosemilla_array["data"]["id"],
                                                "oldshortname" => $body_cursosemilla_array["data"]["shortname"],
                                                "newfullname" => $asignatura['nombre'].'-'.$docente["firstname"].' '.$docente["lastname"],
                                                "newshortname" => $shortname_docente,
                                                "categoryid" => $CATEGORY_ID);                        
                            $failed = false;

                            try {
                                $response_crear_curso = $client->request('POST', $domainws.$ws_virtual.'/api/curso/duplicar',
                                    [ 'json' => $newcurso]);
                            } catch (ClientException $e) {
                                $response = $e->getResponse();
                                $body = $response->getBody();
                                $str_body = (string) $body;
                                $body_php = json_decode($str_body, TRUE);

                                $record_log = ' # '.$s->id.' falla duplicar curso '.$str_body.PHP_EOL;
                                $this->saveLog($fp, $record_log);
                                $failed = true;
                                continue;
                            }
                            
                            if (!$failed) {
                                // success duplicado de curso
                                $record_log = ' # '.$s->id.' curso duplicado '.$shortname_docente.PHP_EOL;
                                $this->saveLog($fp, $record_log);

                                // cambiar estado de solicitud a espacio creado
                                $s->state = '2';
                                $s->save();
                                // matricular docente                                
                                $matricula_docente = array("shortname" => $shortname_docente,
                                                            "username" => $s->username,
                                                            "role" => "teacher",
                                                            "timestart" => $INICIO_MATRICULA,
                                                            "timeend" => $FIN_MATRICULA);
                                $failed = false;
                                try {
                                    $response_matricula_docente = $client->request('POST', $domainws.$ws_virtual.'/api/matricula',
                                        [ 'json' => $matricula_docente]);
                                } catch (ClientException $e) {
                                    $response = $e->getResponse();
                                    $body = $response->getBody();
                                    $str_body = (string) $body;
                                    $body_php = json_decode($str_body, TRUE);
        
                                    $failed = true;
                                    if ($body_php["data"]["status"] == 'enrolled') {
                                        $record_log = ' # '.$s->id.' docente ya esta matriculado '.PHP_EOL;
                                        $this->saveLog($fp, $record_log);
                                    } else {
                                        $record_log = ' # '.$s->id.' falla enroll docente '.$str_body.PHP_EOL;
                                        $this->saveLog($fp, $record_log);
                                        continue;
                                    }
                                }

                                $record_log = ' # '.$s->id.' docente matriculado '.$newcurso["newshortname"].PHP_EOL;
                                $this->saveLog($fp, $record_log);

                                // cambiar estado de la solicitud a docente matriculado
                                $s->state = '3';
                                $s->save();
                            }
                        }
                    }
                    else {
                        // docente tiene su curso
                        $status_code_curso = $response_curso->getStatusCode();
                        $body_curso = $response_curso->getBody();
                        $body_curso_array = json_decode($body_curso, TRUE);
                        $curso_id = $body_curso_array['data']['id'];

                        $record_log = ' # '.$s->id.' extraño docente tiene su curso '.$body_curso_array['data']['shortname'].PHP_EOL;
                        $this->saveLog($fp, $record_log);

                        // cambiar estado de la solicitud
                        $s->state = '3';
                        $s->save();
                    }
                } // job crear espacio

                
                // si solicitud tiene espacio creado entonces matricular
                if ($s->state == '3') {
                    //crear grupos y matricular
                    // formamos el shortname ej: 561231-aaragon-E
                    $shortname_docente = $s->codigo_asignatura.'-'.$s->username.'-'.$s->modalidad;
                    $record_log = ' # '.$s->id.' matricular grupos en '.$shortname_docente.PHP_EOL;
                    $this->saveLog($fp, $record_log);

                    //buscar curso para obtener el id de curso y crear grupos
                    $curso_no_encontrado = false;
                    try {
                        $response_curso = $client->request('GET', $domainws.$ws_virtual.'/api/curso/'.$shortname_docente);
                    } catch (ClientException $e) {
                        // $status_cod_error = Psr7\str($e->getResponse());
                        // docente no tiene curso en siga
                        // no debería entrar aqui sería extraño
                        $response = $e->getResponse();
                        $body = $response->getBody();
                        $str_body = (string) $body;
                        $body_php = json_decode($str_body, TRUE);

                        $record_log = ' # '.$s->id.' curso no encontrado extraño'.$str_body.PHP_EOL;
                        $this->saveLog($fp, $record_log);
                        $curso_no_encontrado = true;
                        continue;
                    }
                    
                    // crear los grupos
                    $status_code_curso = $response_curso->getStatusCode();
                    $body_curso = $response_curso->getBody();
                    $body_curso_array = json_decode($body_curso, TRUE);

                    // obtenemos courseid
                    $courseid = $body_curso_array["data"]["id"];

                    // obtenemos los grupos del curso en SIGA para determinar si cada grupo ha sido creado y evitar q falle
                    $failed_get_grupos = false;
                    try {
                        $response_grupos = $client->request('GET', $domainws.$ws_virtual.'/api/curso/grupos/'.$courseid);
                    } catch (ClientException $e) {
                        $response = $e->getResponse();
                        $body = $response->getBody();
                        $str_body = (string) $body;
                        $body_php = json_decode($str_body, TRUE);
                        $failed_get_grupos = true;
                        $record_log = ' # '.$s->id.' fallo get grupos '.$str_body.PHP_EOL;
                        $this->saveLog($fp, $record_log);
                        continue;
                    }

                    $body_grupos = $response_grupos->getBody();
                    $body_grupos_array = json_decode($body_grupos, TRUE);
                    $grupos_curso = $body_grupos_array["data"]["groups"];

                    $grupos = $s->grupos;
                    // iteramos por los grupos
                    foreach($grupos as $g) {
                        // si el curso no tiene el grupo se crea para evitar una excepcion
                        $grupoExiste = false;
                        $total_grupos = count($grupos_curso);
                        if ($total_grupos > 0) {
                            // ya tiene grupos el curso en siga entonces buscamos si existe
                            foreach ($grupos_curso as $grupo_curso) {
                                $record_log = ' # '.$s->id.' comparo '.$grupo_curso["name"].' con '.$g->grupo.' curso '.$shortname_docente.PHP_EOL;
                                $this->saveLog($fp, $record_log);
                                if ($grupo_curso["name"] == $g->grupo) {
                                    $groupid = $grupo_curso["id"];
                                    $grupoExiste = true;

                                    $record_log = ' # '.$s->id.' grupo '.$g->grupo.' ya existe en '.$shortname_docente.PHP_EOL;
                                    $this->saveLog($fp, $record_log);
                                    break;
                                }
                            }
                        }

                        // grupo no existe, crear grupo
                        if (!$grupoExiste) {
                            // $record_log = ' # '.$s->id.' total grupos: '.$total_grupos.' grupo '.$g->grupo.' no existe '.$shortname_docente.PHP_EOL;
                            // $this->saveLog($fp, $record_log);

                            $gruponew = array("courseid" => $body_curso_array["data"]["id"],
                                                "name" => $g->grupo,
                                                "description" => $g->grupo);
                            
                            try {
                                $response_grupo = $client->request('POST', $domainws.$ws_virtual.'/api/grupo',
                                    [ 'json' => $gruponew]);
                            } catch (ClientException $e) {
                                $response = $e->getResponse();
                                $body = $response->getBody();
                                $str_body = (string) $body;
                                $body_php = json_decode($str_body, TRUE);
                                
                                $record_log = ' # '.$s->id.' fallo creando grupo salta'.$str_body.PHP_EOL;
                                $this->saveLog($fp, $record_log);
                                // abortamos la creación de este grupo
                                continue;
                            }

                            // guardamos el id grupo recien creado
                            $body_group = $response_grupo->getBody();
                            $body_group_array = json_decode($body_group, TRUE);
                            $groupid =  $body_group_array['data']['id'];
                            
                            $record_log = ' # '.$s->id.' grupo '.$g->grupo.' creado en '.$shortname_docente.PHP_EOL;
                            $this->saveLog($fp, $record_log);
                        }
                        
                        //obtenemos los estudiante del grupo para matricular
                        try {
                            $response_estudiantes_grupo = $client->request('GET', $domainws.$ws_academia.'/api/estudiantes/'.$s->codigo_asignatura.'-'.$g->grupo.'/'.$PERIODO_ACTUAL);
                        } catch (ClientException $e) {
                            $response = $e->getResponse();
                            $body = $response->getBody();
                            $str_body = (string) $body;
                            $body_php = json_decode($str_body, TRUE);

                            $record_log = ' # '.$s->id.' falla get students group: '.$g->grupo.$str_body.PHP_EOL;
                            $this->saveLog($fp, $record_log);

                            // abortamos la matriculacion de este grupo
                            // OJO OJO OJO 
                            continue;
                        }
                        
                        $body_estudiantes_grupo = $response_estudiantes_grupo->getBody();
                        $body_estudiantes_grupo_array = json_decode($body_estudiantes_grupo, TRUE);

                        // matriculamos los estudiantes
                        // if (count($body_estudiantes_grupo_array['data']['estudiantes']) == 0) {
                        for ($i = 0; $i < count($body_estudiantes_grupo_array['data']['estudiantes']); $i++) {
                            // si el estudiante no tiene usuario crear cuenta en siga
                            $student_not_found = false;
                            try {
                                $response_get_usuario = $client->request('GET', $domainws.$ws_virtual.'/api/usuario/'.strtolower($body_estudiantes_grupo_array['data']['estudiantes'][$i][0]) );
                            } catch (ClientException $e) {
                                $response = $e->getResponse();
                                $body = $response->getBody();
                                $str_body = (string) $body;
                                $body_php = json_decode($str_body, TRUE);
                                // usuario no existe lo creamos
                                if ($response->getStatusCode() == 404) {
                                    $record_log = ' # '.$s->id.' estudiante no existe '.strtolower($body_estudiantes_grupo_array['data']['estudiantes'][$i][0]).PHP_EOL;
                                    $this->saveLog($fp, $record_log);
                                    $student_not_found = true;
                                }
                            }
                            // creamos el usuario del estudiante
                            if ($student_not_found) {
                                //creamos el usuario
                                $usuario = array("username" => strtolower($body_estudiantes_grupo_array['data']['estudiantes'][$i][0]),
                                                "password" => "Uao.2018",
                                                "nombre" => strtolower($body_estudiantes_grupo_array['data']['estudiantes'][$i][0]),
                                                "apellido" => strtolower($body_estudiantes_grupo_array['data']['estudiantes'][$i][0]),
                                                "email" => strtolower($body_estudiantes_grupo_array['data']['estudiantes'][$i][0]).'@uao.edu.co',
                                                "auth" => "ldap");
                                
                                $create_user = true;

                                try {
                                    $response_create_user = $client->request('POST', $domainws.$ws_virtual.'/api/usuario',
                                                        [ 'json' => $usuario]);
                                } catch (ClientException $e) {
                                    $response = $e->getResponse();
                                    $body = $response->getBody();
                                    $str_body = (string) $body;
                                    $body_php = json_decode($str_body, TRUE);

                                    $record_log = ' # '.$s->id.' fallo creacion usuario '.strtolower($body_estudiantes_grupo_array['data']['estudiantes'][$i][0]).PHP_EOL;
                                    $this->saveLog($fp, $record_log);
                                    $create_user = false;
                                    // saltamos al proximo estudiante
                                    continue;
                                }
                                // obtenemos el id user
                                $body_usuario = $response_create_user->getBody();
                                $body_usuario_array = json_decode($body_usuario, TRUE);

                                $userid = $body_usuario_array['data']['id'];

                                $record_log = ' # '.$s->id.' ok creacion user '.strtolower($body_estudiantes_grupo_array['data']['estudiantes'][$i][0]).PHP_EOL;
                                $this->saveLog($fp, $record_log);
                            } else {
                                // obtenemos el id user
                                $body_usuario = $response_get_usuario->getBody();
                                $body_usuario_array = json_decode($body_usuario, TRUE);

                                $userid = $body_usuario_array['data']['id'];
                            }

                            // se hace enrol al usuario
                            $estudiante = array("shortname" => $shortname_docente,
                                                "username" => strtolower($body_estudiantes_grupo_array['data']['estudiantes'][$i][0]),
                                                "role" => "student",
                                                "timestart" => $INICIO_MATRICULA,
                                                "timeend" => $FIN_MATRICULA);
                            $failed = false;

                            try {                   
                                $response_enrol = $client->request('POST', $domainws.$ws_virtual.'/api/matricula',
                                                [ 'json' => $estudiante]);
                            } catch(ClientException $e) {
                                $response = $e->getResponse();
                                $body = $response->getBody();
                                $str_body = (string) $body;
                                $body_php = json_decode($str_body, TRUE);
                                // here
                                $failed = true;
                                $record_log = ' # '.$s->id.' fallo matriculacion estudiante '.strtolower($body_estudiantes_grupo_array['data']['estudiantes'][$i][0]).PHP_EOL;
                                $this->saveLog($fp, $record_log);
                                // saltamos al siguiente estudiante
                                continue;
                            }

                            // satisfactorio!
                            $record_log = ' # '.$s->id.' estudiante '.$body_estudiantes_grupo_array['data']['estudiantes'][$i][0].' matriculado en '.$shortname_docente.PHP_EOL;
                            $this->saveLog($fp, $record_log);

                            // guardamos estudiante en solicitud-grupos-estudiantes
                            $sge = new solicitud_grupo_estudiante;
                            $sge->grupo_id = $g->id;
                            $sge->username = strtolower($body_estudiantes_grupo_array['data']['estudiantes'][$i][0]);
                            $sge->cod_prog = $body_estudiantes_grupo_array['data']['estudiantes'][$i][1];
                            $sge->ciclo = $body_estudiantes_grupo_array['data']['estudiantes'][$i][2];
                            $sge->save();

                            // agregamos el estudiante al grupo
                            $member = array("groupid" => $groupid, "userid" => $userid);
                            try {
                                $response_addmember = $client->request('POST', $domainws.$ws_virtual.'/api/grupo/add',
                                    ['json' => $member]);
                            } catch (ClientException $e){
                                $response = $e->getResponse();
                                $body = $response->getBody();
                                $str_body = (string) $body;
                                $body_php = json_decode($str_body, TRUE);
                                
                                $record_log = ' # '.$s->id.' fallo add estudiante '.strtolower($body_estudiantes_grupo_array['data']['estudiantes'][$i][0]).' a grupo '.$groupid.PHP_EOL;
                                $this->saveLog($fp, $record_log);
                                // continue fallo agregando al grupo 
                                continue;
                            }
                            $record_log = ' # '.$s->id.' estudiante '.$body_estudiantes_grupo_array['data']['estudiantes'][$i][0].' agregado al grupo '.$groupid.PHP_EOL;
                            $this->saveLog($fp, $record_log);
                        } // for iterando por los estudiantes
                    } // for iterando por los grupos
                    $s->state = '4';
                    $time_unix_current = time();
                    $s->fecha_procesamiento = date("Y-m-d H:i:s", $time_unix_current);
                    $s->save();

                    $record_log = ' # '.$s->id.' solicitud procesada curso '.$shortname_docente.PHP_EOL;
                    $this->saveLog($fp, $record_log);

                    //send email
                    $linkToCampus = $campus_url.'/course/view.php?id='.$courseid;
                    $datos_email_solicitud = array('id' => $s->id, 'username' => $s->username, 'shortname' => $shortname_docente, 'linkToCampus' => $linkToCampus);
                    Mail::to($s->username.'@uao.edu.co')->send(new SolicitudProcesada($datos_email_solicitud));
                    $record_log = ' # '.$s->id.' email enviado a '.$s->username.PHP_EOL;
                    $this->saveLog($fp, $record_log);
                }
            } // loop solicitudes
        } // endif valida parametros
        $record_log = ' ************fin process solicituds**************'.PHP_EOL;
        $this->saveLog($fp, $record_log);
        fclose($fp);
        return;
    }

    public function saveLog($fp, $record_log) {
        $tiempo = date('d-m-Y H:i:s');
        $record_log = $tiempo.$record_log;
        fwrite($fp, $record_log);
        return;
    }
}
