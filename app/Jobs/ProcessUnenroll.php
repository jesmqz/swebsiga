<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Solicitud;
use App\Parametro;
use App\solicitud_grupo_estudiante;
use App\Cancelacion;

use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException;

class ProcessUnenroll implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        date_default_timezone_set('America/Bogota');
        // dominio servicio web
        $domainws = config('swebsiga.domain_ws');
        $ws_academia = config('swebsiga.academia_ws');
        $ws_virtual = config('swebsiga.virtual_ws');
        
        // abrir logs
        $fp = fopen("./log/swebsiga-unenroll.log", "a+");
        $record_log = ' ************BEGIN UNENROLL**************'.PHP_EOL;
        $this->saveLog($fp, $record_log);

        $swon = false;
        /* cargar parametros:
            PERIODO_ACTUAL, INICIO_MATRICULA, FIN_MATRICULA, CATEGORY_ID
        */
        $parametro = Parametro::where('nombre','periodo-actual')->first();
        if (!$parametro) {
            $record_log = ' parametro periodo-actual no encontrado'.PHP_EOL;
            $this->saveLog($fp, $record_log);
        } else {
            $PERIODO_ACTUAL = $parametro->valor;
            $record_log = ' parametro periodo-actual '.$PERIODO_ACTUAL.PHP_EOL;
            $this->saveLog($fp, $record_log);
        }

        $parametro = Parametro::where('nombre','inicio-matricula')->first();
        if (!$parametro) {
            $record_log = ' parametro inicio-matricula no encontrado'.PHP_EOL;
        } else {
            $INICIO_MATRICULA = $parametro->valor;
            $record_log = ' parametro inicio-matricula '.$INICIO_MATRICULA.PHP_EOL;
            $this->saveLog($fp, $record_log);
        }

        $parametro = Parametro::where('nombre','fin-matricula')->first();
        if (!$parametro) {
            $record_log = ' parametro fin-matricula no encontrado'.PHP_EOL;
        } else {
            $FIN_MATRICULA = $parametro->valor;
            $record_log = ' parametro fin-matricula '.$FIN_MATRICULA.PHP_EOL;
            $this->saveLog($fp, $record_log);
        }

        $parametro = Parametro::where('nombre','id-category')->first();
        if (!$parametro) {
            $record_log = ' parametro id-category no encontrado'.PHP_EOL;
        } else {
            $CATEGORY_ID = $parametro->valor;
            $record_log = ' parametro id-category '.$CATEGORY_ID.PHP_EOL;
            $this->saveLog($fp, $record_log);
        }

        // validamos la parametrizacion 
        if (isset($PERIODO_ACTUAL) && isset($INICIO_MATRICULA) && isset($FIN_MATRICULA) && isset($CATEGORY_ID)) {
            // obtenemos las solicitudes OK para actualizarlas
            $solicitudes = Solicitud::where('periodo', $PERIODO_ACTUAL)->where('state', '8')->get();

            $client = new \GuzzleHttp\Client();
            $record_log = ' #  BEGIN '.PHP_EOL;
            $this->saveLog($fp, $record_log);
            // recorremos las solicitudes
            foreach($solicitudes as $s) {
                $s->state = '3';
                $s->save();
                // formamos el shortname ej: 561231-aaragon-E
                $shortname_docente = $s->codigo_asignatura.'-'.$s->username.'-'.$s->modalidad;
                $record_log = ' # '.$s->id.' desmatricular en '.$shortname_docente.PHP_EOL;
                $this->saveLog($fp, $record_log);
                //buscar curso para obtener el id de curso y crear grupos
                $curso_no_encontrado = false;
                try {
                    $response_curso = $client->request('GET', $domainws.$ws_virtual.'/api/curso/'.$shortname_docente);
                } catch (ClientException $e) {
                    // $status_cod_error = Psr7\str($e->getResponse());
                    // docente no tiene curso en siga
                    // no debería entrar aqui sería extraño
                    $response = $e->getResponse();
                    $body = $response->getBody();
                    $str_body = (string) $body;
                    $body_php = json_decode($str_body, TRUE);

                    $record_log = ' # '.$s->id.' curso no encontrado extraño'.$str_body.PHP_EOL;
                    $this->saveLog($fp, $record_log);
                    $curso_no_encontrado = true;
                    // saltamos la solicitud
                    continue;
                }
                $status_code_curso = $response_curso->getStatusCode();
                $body_curso = $response_curso->getBody();
                $body_curso_array = json_decode($body_curso, TRUE);
                // obtenemos courseid
                $courseid = $body_curso_array["data"]["id"];

                //obtenemos estudiantes matriculados en el curso en campus
                try {
                    $response_estudiantes_campus = $client->request('GET', $domainws.$ws_virtual.'/api/curso/estudiantes/'.$courseid);
                } catch(ClientException $e) {
                    $response = $e->getResponse();
                    $body = $response->getBody();
                    $str_body = (string) $body;
                    $body_php = json_decode($str_body, TRUE);

                    if ($body_php['data']['status'] == "not_found") {
                        $record_log = ' # '.$s->id.' no tiene estudiantes '.PHP_EOL;
                        $this->saveLog($fp, $record_log);
                    }
                    // saltamos la solicitud
                    continue;
                }
                $body_estudiantes_campus = $response_estudiantes_campus->getBody();
                $body_estudiantes_campus_array = json_decode($body_estudiantes_campus, TRUE);

                // iteramos por los grupos
                $grupos = $s->grupos;
                $estudiantes_academia = array();
                foreach($grupos as $g) {
                    // obtenemos estudiantes del curso en academia
                    try {
                        $response_estudiantes_academia = $client->request('GET', $domainws.$ws_academia.'/api/estudiantes/'.$s->codigo_asignatura.'-'.$g->grupo.'/'.$PERIODO_ACTUAL);
                    } catch (ClientException $e) {
                        $response = $e->getResponse();
                        $body = $response->getBody();
                        $str_body = (string) $body;
                        $body_exception = json_decode($body, TRUE);

                        $entro = true;  

                        if ($body_exception['data']['status'] == "not-found") {
                            $record_log = ' # '.$s->id.' no tiene estudiantes '.PHP_EOL;
                            $this->saveLog($fp, $record_log);
                        }
                        continue;
                    }
                    $body_estudiantes_academia = $response_estudiantes_academia->getBody();
                    $body_estudiantes_academia_array = json_decode($body_estudiantes_academia, TRUE);

                    for($i = 0; $i < count($body_estudiantes_academia_array['data']['estudiantes']); $i++) {
                        $estudiantes_academia[] = $body_estudiantes_academia_array['data']['estudiantes'][$i];
                    }
                    
                } // itera grupos

                // buscar quien no se encuentar en 
                for($i = 0; $i < count($body_estudiantes_campus_array['data']['students']); $i++) {
                    $clave = false;
                    $clave = array_search(strtoupper($body_estudiantes_campus_array['data']['students'][$i]), $estudiantes_academia);
                    if ($clave === false) {
                        $record_log = ' # '.$s->id.' '.strtoupper($body_estudiantes_campus_array['data']['students'][$i]).' NOT FOUND IN ACADEMIA '.$g->grupo.PHP_EOL;
                        $this->saveLog($fp, $record_log);
                        $sge = null;
                        foreach($grupos as $g) {
                            $estudiantes = $g->estudiantes;
                            $sges = $estudiantes->where('username', $body_estudiantes_campus_array['data']['students'][$i]);
                            foreach($sges as $sge) {
                                solicitud_grupo_estudiante::destroy($sge->id);
                                $record_log = ' id sge: '.$sge->id.' student: '.$body_estudiantes_campus_array['data']['students'][$i].PHP_EOL;
                                $this->saveLog($fp, $record_log);
                            }
                        }
                        // desmatricular
                        $matricula = array("shortname" => $shortname_docente,
                                            "username" => $body_estudiantes_campus_array['data']['students'][$i]);
                        try {
                            $response_unenroll = $client->request('DELETE', $domainws.$ws_virtual.'/api/matricula',
                                ['json' => $matricula]);
                        } catch(ClientException $e) {
                            $response = $e->getResponse();
                            $body = $response->getBody();
                            $str_body = (string) $body;
                            $body_php = json_decode($str_body, TRUE);

                            $record_log = ' # '.$s->id.' falla unenroll '.$str_body.PHP_EOL;
                            $this->saveLog($fp, $record_log);
                            
                            continue;
                        }
                        $record_log = ' # '.$s->id.' '.strtoupper($body_estudiantes_campus_array['data']['students'][$i]).' UNENROLLED '.$g->grupo.PHP_EOL;
                        $this->saveLog($fp, $record_log);
                        // crear la cancelación
                        $cancelacion = new Cancelacion;
                        $cancelacion->username = $body_estudiantes_campus_array['data']['students'][$i];
                        $cancelacion->codigo_asignatura = $s->codigo_asignatura;
                        $cancelacion->solicitud_id = $s->id;
                        $cancelacion->save();
                    }
                }
                $s->state = '4';
                $s->save();
            } //itera solicituds
            $record_log = ' ************ending process unenroll**************'.PHP_EOL;
            $this->saveLog($fp, $record_log);
            fclose($fp);
        }
    }

    public function saveLog($fp, $record_log) {
        $tiempo = date('d-m-Y H:i:s');
        $record_log = $tiempo.$record_log;
        fwrite($fp, $record_log);
        return;
    }

}
