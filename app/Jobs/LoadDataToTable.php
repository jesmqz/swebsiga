<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Solicitud;
use App\Grupo;
use App\Parametro;
use App\solicitud_grupo_estudiante;

use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException;

class LoadDataToTable implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();

        date_default_timezone_set('America/Bogota');
        // dominio servicio web
        $domainws = config('swebsiga.domain_ws');
        $ws_academia = config('swebsiga.academia_ws');

        // abrir logs
        $fp = fopen("./log/swebsiga-loaddata.log", "a+");
        $record_log = ' ************inicio**************'.PHP_EOL;
        $this->saveLog($fp, $record_log);
        
        $solicitudes = Solicitud::all();
        // $solicitudes = Solicitud::where('periodo', '2019-03')->get();
        foreach( $solicitudes as $s) {
            // $record_log = ' id: '.$s->id.' shortname: '.$s->codigo_asignatura.'-'.$s->username.'-'.$s->modalidad.PHP_EOL;
            // $this->saveLog($fp, $record_log);
            $grupos = $s->grupos;
            foreach($grupos as $g) {
                // $str_grupo = str_replace(" ", "-", $g->grupo);
                $codigo_curso = $s->codigo_asignatura.'-'.$g->grupo;
                $periodo = $s->periodo;
                try {
                    $response_estudiantes = $client->request('GET', $domainws.$ws_academia.'/api/estudiantes/'.$codigo_curso.'/'.$periodo);
                
                    $body_response = $response_estudiantes->getBody();
                    $body_response_array = json_decode($body_response, TRUE);
                    $estudiantes_academia = $body_response_array["data"]["estudiantes"];
    
                    $usernames = array_column($estudiantes_academia, '0');
                    $estudiantes_sga = $g->estudiantes;
                    foreach($estudiantes_sga as $e) {
                        if ($e->username === '') continue;
                        $key = array_search(strtoupper($e->username), $usernames);
                        // to update record sge
                        $e->cod_prog = $estudiantes_academia[$key][1];
                        $e->ciclo = $estudiantes_academia[$key][2];
                        $e->save();
    
                        $record_log = ' id: '.$s->id.' shortname: '.$s->codigo_asignatura.'-'.$s->username.'-'.$s->modalidad.' grupo: '.$g->grupo.' username: '.$e->username.' prg: '.$estudiantes_academia[$key][1].' ciclo: '.$estudiantes_academia[$key][2].PHP_EOL;
                        $this->saveLog($fp, $record_log);
                    }

                } catch (ClientException $e) {
                    $record_log = ' fault id: '.$s->id.' shortname: '.$s->codigo_asignatura.'-'.$s->username.'-'.$s->modalidad.' grupo: '.$g->grupo.PHP_EOL;
                    $this->saveLog($fp, $record_log);
                    $response = $e->getResponse();
                    $body = $response->getBody();
                    $str_body = (string) $body;
                    $body_php = json_decode($str_body, TRUE);
            
                    if ($response->getStatusCode() == 404) {
                        // $record_log = ' curso no encontrado academia '.$body_php['data']['message'].PHP_EOL;
                        $record_log = ' fault id: '.$s->id.' shortname: '.$s->codigo_asignatura.'-'.$s->username.'-'.$s->modalidad.' grupo: '.$g->grupo.PHP_EOL;
                    } else {
                        $record_log = ' error webservice '.$body_php['data']['message'].PHP_EOL;
                    }
                    continue;
                }
            } // for grupos
        } // for solicitudes
        $record_log = '********process finished*******'.PHP_EOL;
        $this->saveLog($fp, $record_log);
        fclose($fp);
        return;
    }

    public function saveLog($fp, $record_log) {
        $tiempo = date('d-m-Y H:i:s');
        $record_log = $tiempo.$record_log;
        fwrite($fp, $record_log);
        return;
    }
}
