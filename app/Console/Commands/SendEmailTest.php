<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mail\SolicitudProcesada;
use Illuminate\Support\Facades\Mail;

class SendEmailTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:send {user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email to a user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $username = $this->argument('user');
        $email = $username.'@uao.edu.co';
        $shortname_docente = 'curso-test-uaovirtual';
        // test course id
        $courseid = 50;
        $campus_url = config('swebsiga.campus_url');
        $linkToCampus = $campus_url.'/course/view.php?id='.$courseid;
        $datos_email_solicitud = array('id' => '9999', 'username' => $username, 'shortname' => $shortname_docente, 'linkToCampus' => $linkToCampus);
        Mail::to($email)->send(new SolicitudProcesada($datos_email_solicitud));
        $this->info('Email was sent to '.$username.'!');
    }
}
