<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App\Jobs\ProcessSolicitud;
use App\Jobs\ProcessUpdateEnroll;
use App\Jobs\ProcessUnenroll;
use App\Jobs\ProcessUnenrollmentAcademy;
use App\Jobs\LoadDataToTable;

use App\Parametro;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // hacer registro logs
        $fp = fopen("./log/kernel.log", "a+");

        $parametro = Parametro::where('nombre', 'hora-exec-solicitudes')->first();
        if (!$parametro) {
            $record_log = ' parametro hora-exec-solicitudes no encontrado'.PHP_EOL;
            $this->saveLog($fp, $record_log);
        } else {
            $hora_exec_solicitudes = $parametro->valor;
            if ($hora_exec_solicitudes == 'none') {
                $record_log = ' none process solicituds'.PHP_EOL;
                $this->saveLog($fp, $record_log);
            } else {
                $record_log = ' process solicituds at:'.$hora_exec_solicitudes.PHP_EOL;
                $this->saveLog($fp, $record_log);
                $schedule->job(new ProcessSolicitud)->dailyAt($hora_exec_solicitudes)->timezone('America/Bogota');     
            }
        }

        $parametro = Parametro::where('nombre', 'hora-exec-updates')->first();
        if (!$parametro) {
            $record_log = ' parametro hora-exec-updates no encontrado'.PHP_EOL;
            $this->saveLog($fp, $record_log);
        } else {
            $hora_exec_updates = $parametro->valor;
            if ($hora_exec_updates == 'none') {
                $record_log = ' none process updates'.PHP_EOL;
                $this->saveLog($fp, $record_log);
            } else {
                $record_log = ' process updates at:'.$hora_exec_updates.PHP_EOL;
                $this->saveLog($fp, $record_log);
                $schedule->job(new ProcessUpdateEnroll)->dailyAt($hora_exec_updates)->timezone('America/Bogota');
            }
        }

        $parametro = Parametro::where('nombre', 'hora-exec-unenrolls')->first();
        if (!$parametro) {
            $record_log = ' parametro hora-exec-unenrolls no encontrado'.PHP_EOL;
            $this->saveLog($fp, $record_log);
        } else {
            $hora_exec_unenrolls = $parametro->valor;
            if ($hora_exec_unenrolls == 'none') {
                $record_log = ' none process unenrolls'.PHP_EOL;
                $this->saveLog($fp, $record_log);
            } else {
                $record_log = ' process unenroll at: '.$hora_exec_unenrolls.PHP_EOL;
                $this->saveLog($fp, $record_log);
                $schedule->job(new ProcessUnenrollmentAcademy)->dailyAt($hora_exec_unenrolls)->timezone('America/Bogota');
            }
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }

    public function saveLog($fp, $record_log) {
        $tiempo = date('d-m-Y H:i:s');
        $record_log = $tiempo.$record_log;
        fwrite($fp, $record_log);
        return;
    }
}
