
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.events = new Vue();

window.flash = function (message) {
  window.events.$emit('flash', message)
};

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
var myMixin = {
  data: function () {
    return {
      url: '#'
    }
  }
}

Vue.component('courses-tables', require('./components/CoursesTableComponent.vue'));
Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('flashmessage-component', require('./components/FlashMessageComponent.vue'));
Vue.component('deletemodal-component', require('./components/DeleteUserComponent.vue'));
Vue.component('button-delete-component', require('./components/ButtonDeleteComponent.vue'));
Vue.component('chart-requests-home', require('./components/GraphicRequest.vue'));
Vue.component('chart-courses-home', require('./components/GraphicCourses.vue'));

const app = new Vue({
    el: '#app',
    mixins: [myMixin]
});
