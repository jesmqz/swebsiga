@extends('layouts.app')
@section('content')
<div class="container cit-content">
    <div class="container cit-welcome-container-text">
            <h5>Sistema de Gestión Administrativo - UAO Virtual</h5>
            <h3> Período actual <a class="alert-link">{{ $periodo }}</a></h3>
    </div>


    <div class="row pt-sm-3">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        @if(Auth::user()->hasRole('admin'))
            <div class="col-sm-12">
                <div class="card-deck">
                    <div class="card shadow cit-dashboard-card">
                        <div class="card-body">
                            <!--<h2 class="card-title">{{ $totales['total_solicitudes'] }} solicitudes</h2>
                            <p class="card-text font-weight-bold">{{ $totales['total_solicitudes_ok'] }} procesadas</p>
                            <p class="card-text font-weight-bold">{{ $totales['total_solicitudes_por_procesar'] }} sin procesar</p>
                            <p class="card-text font-weight-bold">{{ $totales['total_solicitudes_updating'] }} actualizandose</p>
                            <p class="card-text font-weight-bold">{{ $totales['total_solicitudes_pausadas'] }} pausadas</p>
                            <p class="card-text font-weight-bold">{{ $totales['total_solicitudes_update'] }} para actualizar </p>
                            <p class="card-text font-weight-bold">{{ $totales['total_solicitudes_admiracion'] }} revisión </p>-->
                            <chart-requests-home 
                            v-bind:nutotalrequests="{{ $totales['total_solicitudes'] }}" 
                            v-bind:nuokprocess="{{ $totales['total_solicitudes_ok'] }}"
                            v-bind:nuupdating="{{ $totales['total_solicitudes_updating'] }}"
                            v-bind:nupaused="{{ $totales['total_solicitudes_pausadas'] }}"
                            v-bind:nupendingupdate="{{ $totales['total_solicitudes_update'] }}"
                            v-bind:nurevision="{{ $totales['total_solicitudes_admiracion'] }}">
                            </chart-requests-home>
                            <a href="{{ route('solicitudes')}}" class="btn cit-btn cit-btn-sm">Ver</a>
                        </div>
                    </div>

                    <div class="card shadow cit-dashboard-card cit-dashboard-card-small">
                        <div class="card-body">
                            <chart-courses-home 
                                v-bind:nutotalcourses="{{ $totales['total_solicitudes'] }}" 
                                v-bind:nutotalelearning="{{ $num_cursos_e }}"
                                v-bind:nutotalblearning="{{ $num_cursos_b }}">
                            </chart-courses-home>
                        </div>

                        
                    </div>

                    
                    <div class="card shadow cit-dashboard-card">
                        <div class="card-body">
                            <h2 class="card-title">Reportes</h2>
                            <p class="card-text font-weight-bold">Balance general</p>
                            <p class="card-text font-weight-bold">Comparativo oferta por modalidad</p>
                            <p class="card-text font-weight-bold">Comparativo oferta E-learning por facultad</p>
                            <p class="card-text font-weight-bold">Detalle oferta de cursos E-learning</p>
                            <p class="card-text font-weight-bold">Comparativo oferta B-learning por facultad</p>
                            <p class="card-text font-weight-bold">Detalle oferta de cursos B-learning</p>
                            <p class="card-text font-weight-bold">Cursos B-learning por facultad</p>
                            <p class="card-text font-weight-bold">Estudiantes B-learning por facultad</p>
                            <p class="card-text font-weight-bold">Cursos B-learning por departamento por facultad</p>
                            <p class="card-text font-weight-bold">Estudiantes en B-learning por programa</p>
                            <a id="button-cargando" href="{{ route('reportes') }}" class="btn cit-btn cit-btn-sm">Ver</a>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-sm-12 pt-sm-5">
                <div class="card-deck">
                    <div class="card shadow cit-dashboard-card">
                        <div class="card-body">
                            <h2 class="card-title">Uso</h2>
                            <p class="card-text font-weight-bold">190 autenticaciones</p>
                            <p class="card-text font-weight-bold">15 blearning</p>

                            <a href="#" class="btn cit-btn cit-btn-sm">Ver</a>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div>Acceso como guess</div>
        @endif
    </div> <!-- cards dashboard  -->
</div>
@endsection
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
    $('#button-cargando').on('click', function() {
        var $this = $(this);
        var loadingText = '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>  Cargando...';
        if ($(this).html() !== loadingText) {
        $this.data('original-text', $(this).html());
        $this.html(loadingText);
        }
    });
    })
</script>
