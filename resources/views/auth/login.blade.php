@extends('layouts.app')

@section('content')
<div class="container cit-content">
    <div class="row justify-content-center pt-sm-5">
        <div class="col-md-8">
            <div class="card cit-login-card">
                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group column">
                            <label for="email" class="col-sm-12 col-form-label text-md-left">{{ __('Correo electrónico') }}</label>
                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group column">
                            <label for="password" class="col-md-12 col-form-label text-md-left">{{ __('Contraseña') }}</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                         <div class="form-group column mb-0">
                            <div class="col-md-12 offset-md-12">
                                <button type="submit" class="btn cit-btn">
                                    {{ __('Iniciar sesión') }}
                                </button>                             
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
