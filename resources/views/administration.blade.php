@extends('layouts.app')

@section('content')
    <div class="container cit-content">
        <div class="row justify-content-center no-gutters">
            <div class="col-md-10">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent">
                  <li class="breadcrumb-item"><a href="{{ route('home') }}" class="text-dark">Inicio</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Administración</li>
                </ol>
              </nav>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="list-group">
                    <a href="{{route('users')}}" class="list-group-item list-group-item-action">Usuarios</a>
                    <a href="{{route('periodos')}}" class="list-group-item list-group-item-action">Períodos</a>
                    <a href="{{route('parametros')}}" class="list-group-item list-group-item-action">Parámetros</a>
                </div>
            </div>
        </div>
    </div>
@endsection