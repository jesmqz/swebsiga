@extends('layouts.app2')

@section('content')
  <div class="container cit-content">
    <div class="row align-items-center justify-content-center" style="margin-top:50px">
        <div class="col-md-12">
          @if ($periodo == 'none')
            <div class="alert alert-danger" role="alert">
              Se encuentra inhabilitado el proceso de solicitudes de ambientes electrónicos.
            </div>
          @else
            <p class="font-weight-bold">Período académico: {{ $periodo }}</p>
            <p>Paso 1: Ingrese el email UAO y el código de asignatura para crear la solicitud de actividad de ambientes electrónicos para cursos en las modalidades e-learning (cursos virtuales), b-learning (cursos apoyados por TIC), y el registros de estudiantes en la plataforma.</p>
            <div class="alert alert-danger" role="alert">
              Si usted es Coordinador o Jefe de Departamento, tenga en cuenta que debe diligenciar el formulario una vez por cada asignatura y por cada docente. Es decir, si desea realizar algún tipo de solicitud vinculada a los cursos de su área, deberá rellenar el formulario por cada docente que tenga a cargo. 
            </div>
            <!--
            @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
            @endif
            -->
            @if(Session::has('message'))
                <div class="alert alert-warning" role="alert">
                {{ Session::get('message') }}
                </div>
            @endif
            <form action="{{ route('redirigirCrearSolicitud') }}" method="GET">
                <div class="form-row"> 
                    <div class="form-group col-md-6">
                        <label for="email">Email Docente</label>
                        <input type="email" name="email" id="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="Ingrese el email institucional" required autofocus>
                        @if ($errors->has('email'))
                          <span class="invalid-feedback">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                        @endif                      
                    </div>
                    <div class="form-group col-md-6">
                        <label for="codigo">Código Asignatura</label>
                        <input type="text" name="codigo" id="codigo" class="form-control{{ $errors->has('codigo') ? ' is-invalid' : '' }}"  value="{{ old('codigo') }}" placeholder="Ingrese código asignatura" required autofocus>
                        @if ($errors->has('codigo'))
                          <span class="invalid-feedback">
                              <strong>{{ $errors->first('codigo') }}</strong>
                          </span>
                        @endif             
                    </div>
                    <button type="submit" class="btn cit-btn cit-btn-sm">Crear la solicitud</button>
                </div>
            </form>
          @endif
        </div>
    </div>
  </div>
@endsection