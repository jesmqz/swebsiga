@extends('layouts.app')

@section('content')
    <div class="container cit-content">
        <div class="row justify-content-center no-gutters">
            <div class="col-md-10">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent">
                <li class="breadcrumb-item"><a href={{ route('home') }} class="text-dark">Home</a></li>
                <li class="breadcrumb-item"><a href={{ route('solicitudes') }} class="text-dark">Solicitudes</a></li>
                <li class="breadcrumb-item active" aria-current="page">Grupo</li>
                </ol>
            </nav>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                <div class="card-header">Solicitud: {{$group->solicitud_id}} Grupo {{ $group->grupo }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('solicitud.destroyGroup', ['id_solicitud' => $group->id ]) }}">
                            @csrf
                            @method('DELETE')
                            <input type="hidden" value="1" name="prueba">
                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4" style="padding-left: 0px; padding-right: 0px; height:400px;">
                                    <label for="exampleFormControlSelect2">{{ $group->estudiantes->count() }} estudiantes:</label>
                                    <select multiple class="form-control" id="exampleFormControlSelect2" style="height: 370px;">
                                        @foreach ($students as $student)
                                            <option>{{ $student->username }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4 alert alert-danger" role="alert">
                                    !Ingrese ELIMINAR para confirmar!
                                    <input id="txtConfirmacion" type="text" class="form-control" onkeyUP="validate()">
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button id="btnsubmit" type="submit" class="btn btn-outline-danger" disabled>ELIMINAR</button>
                                    <a class="btn btn-primary" href={{ route('solicitudes')}} role="button">Cancelar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div> <!-- card -->
            </div>
        </div>
    </div>
@endsection
<script src="{{ asset('js/custom.js') }}"></script>