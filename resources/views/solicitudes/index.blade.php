@extends('layouts.app')

@section('content')
    {{-- breadcrumb --}}
    <div class="container cit-content">
        <div class="row justify-content-center no-gutters">
            <div class="col-md-10">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent">
                <li class="breadcrumb-item"><a href={{ route('home') }} class="text-dark">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Solicitudes</li>
                </ol>
            </nav>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row pt-sm-3">
            <div class="col-md-12 d-flex justify-content-center">
                <p class="h4">solicitudes {{ $periodo_actual }}</p>
            </div>
        </div>
        <div class="row pt-sm-3">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col" style="width: 2%">#</th>
                                <th scope="col" style="width: 20%"><i class="fas fa-user"></i></th>
                                <th scope="col" style="width: 10%"><i class="fas fa-book"></i></th>
                                <th scope="col" style="width: 10%">modalidad</th>
                                <th scope="col" style="width: 20%"><i class="fas fa-users"></i></th>
                                <th scope="col" style="width: 10%"><i class="fas fa-battery-three-quarters"></i></th>
                                <th scope="col" style="width: 10%"><i class="fas fa-calendar-alt"></i></th>
                                <th scope="col" style="width: 10%"><i class="fas fa-hourglass-end"></i></th>
                                <th scope="col" colspan="3" style="width: 8%">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $i=1;
                            @endphp
                            @foreach ($solicitudes as $solicitud)
                                <tr>
                                    <th scope="row" class="table-light">{{ $solicitud->id }}</th>
                                    <td class="table-light">{{ $solicitud->username }}</td>
                                    <td class="table-light">{{ $solicitud->codigo_asignatura }}</td>
                                    <td class="table-light">{{ $solicitud->modalidad }}</td>
                                    <td class="table-light">
                                        @foreach ($solicitud->grupos as $g)
                                            <a href={{ route('solicitud.showGroup', ['id_group' => $g->id]) }}>{{ $g->grupo }}</a>
                                        @endforeach
                                    </td>
                                    
                                    @switch($solicitud->state)
                                        @case(0)
                                            <td class="table-light"><i class="fas fa-sign-in-alt"></i></i></td>
                                            @break
                                        @case(1)
                                            <td class="table-light"><i class="fas fa-building"></i></td>
                                            @break
                                        @case(2)
                                            <td class="table-light"><i class="fas fa-clipboard-check"></i></td>
                                            @break
                                        @case(3)
                                            <td class="table-light"><i class="fas fa-sync"></i></td>
                                            @break
                                        @case(4)
                                            <td class="table-light"><i class="far fa-smile"></i></td>
                                            @break
                                        @case(5)
                                            <td class="table-light"><i class="fas fa-pause"></i></td>
                                            @break
                                        @case(6)
                                            <td class="table-light"><i class="fas fa-exclamation"></i></td>
                                            @break
                                        @case(7)
                                            <td class="table-light"><i class="fas fa-upload"></i></td>
                                            @break
                                        @case(8)
                                            <td class="table-light"><i class="fas fa-eraser"></i></td>
                                            @break

                                    @endswitch
                                    <td class="table-light">{{ $solicitud->fecha_solicitud }}</td>
                                    @if ($solicitud->fecha_procesamiento == null )
                                        <td class="table-light"><i class="far fa-clock"></i></td>
                                    @else
                                        <td class="table-light">{{ $solicitud->fecha_procesamiento }}</td>
                                    @endif
                                    <td class="actions"><a href={{ route('solicitud.redirectToCampus', ['shortname' => $solicitud->codigo_asignatura.'-'.$solicitud->username.'-'.$solicitud->modalidad ]) }} target="_blank"><i class="fas fa-university"></i></a></td>
                                    <td class="actions"><a href={{ route('solicitud.edit', ['id_solicitud' => $solicitud->id])}} class="icon"><i class="far fa-edit"></i></a></td>
                                    <td class="actions"><a href={{ route('solicitud.show', ['id_solicitud' => $solicitud->id])}} class="icon"><i class="fas fa-trash"></i></a></td>
                                </tr>
                                @php
                                    $i++;
                                @endphp
                            @endforeach
                        </tbody>
                    </table>
                </div> <!-- table -->
            </div> <!-- col -->
        </div> <!-- row -->
    </div> <!-- container -->
@endsection