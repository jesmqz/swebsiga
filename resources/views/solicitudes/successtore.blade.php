@extends('layouts.app2')

@section('content')
    <div class="container cit-content">
        <div class="row justify-content-center" style="margin-top:50px">
            <div class="card shadow cit-dashboard-card">
                <h5 class="card-header">Activación y matriculación</h5>
                <div class="card-body">
                    <h5 class="card-title">Enviado satisfactoriamente</h5>
                    <p class="card-text">Su solicitud ha sido registrada en el sistema.</p>
                    <a href="{{ route('consultarGruposAsignaturaDocente') }}" class="btn cit-btn cit-btn-sm">Activar otro curso</a>
                </div>
            </div>
        </div>
    </div>
@endsection