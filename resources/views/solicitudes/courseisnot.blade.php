@extends('layouts.app2')
@section('content')
    <div class="container cit-content">
        <div class="row justify-content-center" style="margin-top:50px">
            <div class="card shadow cit-dashboard-card">
                <h5 class="card-header">REQUERIMIENTO NO ENCONTRADO</h5>
                <div class="card-body">
                    <h5 class="card-title">Curso no se encuentra en campus.</h5>
                    <p class="card-text">El curso con el shortname {{ $shortname }} no se encuentra en campus.</p>
                    <a href="{{ route('solicitudes') }}" class="btn cit-btn cit-btn-sm">Regresar a solicitudes</a>
                </div>
            </div>
        </div>
    </div>
@endsection