@extends('layouts.app')

@section('content')
<div class="container cit-content">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">ELIMINAR SOLICITUD</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('solicitud.destroy', ['id_solicitud' => $solicitud->id ]) }}">
                        @csrf
                        @method('DELETE')
                        <input type="hidden" value="1" name="prueba">
                        <div class="form-group row">
                            <label for="id" class="col-md-4 col-form-label text-md-right">{{ __('id') }}</label>
                            <div class="col-md-6">
                                <input id="id" class="form-control{{ $errors->has('id') ? ' is-invalid' : '' }}" name="id" value="{{ $solicitud->id }}" required readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('username') }}</label>
                            <div class="col-md-6">
                                <input id="username" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" value="{{ $solicitud->username }}" name="username" required readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="codigo_asignatura" class="col-md-4 col-form-label text-md-right">{{ __('codigo_asignatura') }}</label>
                            <div class="col-md-6">
                                <input id="codigo_asignatura" class="form-control{{ $errors->has('codigo_asignatura') ? ' is-invalid' : '' }}" value="{{ $solicitud->codigo_asignatura }}" name="codigo_asignatura" required readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="modalidad" class="col-md-4 col-form-label text-md-right">{{ __('modalidad') }}</label>
                            <div class="col-md-6">
                                <input 
                                    id="modalidad"
                                    class="form-control{{ $errors->has('modalidad') ? ' is-invalid' : '' }}"
                                    value="{{ $solicitud->modalidad }}"
                                    name="modalidad" required readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="facultad" class="col-md-4 col-form-label text-md-right">{{ __('facultad') }}</label>
                            <div class="col-md-6">
                                <input id="facultad" class="form-control{{ $errors->has('facultad') ? ' is-invalid' : '' }}" value="{{ $solicitud->facultad }}" name="facultad" required readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="departamento" class="col-md-4 col-form-label text-md-right">{{ __('departamento') }}</label>
                            <div class="col-md-6">
                                <input id="departamento" class="form-control{{ $errors->has('departamento') ? ' is-invalid' : '' }}" value="{{ $solicitud->departamento }}" name="departamento" required readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="state" class="col-md-4 col-form-label text-md-right">{{ __('estado') }}</label>
                            <div class="col-md-6">
                                <input id="state" class="form-control{{ $errors->has('state') ? ' is-invalid' : '' }}" value="{{ $solicitud->state }}" name="state" required readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4 alert alert-danger" role="alert">
                                !Ingrese ELIMINAR para confirmar!
                                <input id="txtConfirmacion" type="text" class="form-control" onkeyUP="validate()">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button id="btnsubmit" type="submit" class="btn btn-outline-danger" disabled>ELIMINAR</button>
                                <a class="btn btn-primary" href={{ route('solicitudes')}} role="button">Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div> <!-- card -->
        </div> <!-- col-md-8 --> 
    </div>
</div>
@endsection
<script src="{{ asset('js/custom.js') }}"></script>