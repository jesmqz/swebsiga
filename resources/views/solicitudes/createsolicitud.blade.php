@extends('layouts.app2')

@section('content')
    <div class="container cit-content">
        <div class="row justify-content-center" style="margin-top:50px">
            <div class="col-md-12">
                <form action="{{ route('solicitud.store') }}" method="POST">
                    @csrf
                    <input type="hidden" name="username" value="{{ $docente['username'] }}">
                    <input type="hidden" name="cod_asignatura" value="{{ $asignatura['codigo'] }}">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="nombreDocenteInput">Nombre</label>
                            <input class="form-control" placeholder="{{ $docente['firstname'] }}" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="apellidoDocenteInput">Apellido</label>
                            <input class="form-control" placeholder="{{ $docente['lastname'] }}" readonly>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="formGroupExampleInput">Facultad</label>
                            <input name="facultad" class="form-control" id="nombreFacultadInput" name="facultad" type="text" value="{{ $asignatura['facultad'] }}" placeholder="{{ $asignatura['facultad'] }}" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="formGroupExampleInput">Departamento</label>
                            <input name="departamento" class="form-control" id="nombreDepartamentoInput" type="text" value="{{ $asignatura['departamento'] }}" placeholder="{{ $asignatura['departamento'] }}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nameAsignaturaInput">Nombre de la asignatura</label>
                        <input class="form-control" id="nombreAsignaturaInput" type="text" placeholder="{{ $asignatura['nombre'] }}" readonly>
                    </div>
                    <div class="form-group">
                        <p class="font-weight-bold">Paso 2:</p>
                        <p>Estimado docente {{ $docente['firstname'] }}:</p>

                        <p>El código asignatura {{ $asignatura['codigo'] }} tiene los siguientes grupos. Por favor seleccione los grupos y haga click en el botón Enviar la solicitud.</p>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2">Grupos</div>
                        <div class="col-sm-10">
                            @php
                            $i=0;
                            $swAlertMessageTaken = false;
                            @endphp
                            @foreach ($grupos as $grupo)
                                @php
                                    $swtaken=false;
                                    foreach ($solicitudes as $s) {
                                        $gruposTaken = $s->grupos;
                                        foreach ($gruposTaken as $grupoTaken) {
                                            if ($grupoTaken->grupo == $grupo) {
                                                $swtaken = true;
                                                $swAlertMessageTaken = true;
                                                break;
                                            }
                                        }
                                    }
                                @endphp
                                @if ($swtaken)
                                    <div class="form-check">
                                        <input class="form-check-input" name="grupos[]" type="checkbox" id="gridCheck{{ $i }}1" value="{{ $grupo }}" disabled>
                                        <label class="form-check-label" for="gridCheck{{ $i }}1">{{ $grupo }}</label>                                
                                    </div>
                                @else
                                    <div class="form-check">
                                        <input class="form-check-input" name="grupos[]" type="checkbox" id="gridCheck{{ $i }}1" value="{{ $grupo }}">
                                        <label class="form-check-label" for="gridCheck{{ $i }}1">{{ $grupo }}</label>                                
                                    </div>
                                @endif
                                @php $i++; @endphp
                            @endforeach
                        </div>
                    </div>
                    @if ($swAlertMessageTaken)
                      <div class="form-group">
                          <div class="alert alert-warning" role="alert">
                              Los grupos inhabilitados ya se encuentra solicitado por otro docente. Comuniquese con el CIT extensión 12420 para cualquier inquietud.
                          </div>
                      </div>
                    @endif
                    <button type="submit" id="btn" class="btn cit-btn cit-btn-sm" disabled>Enviar solicitud</button>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scriptjs')
<script type="text/javascript">
  $( document ).ready(function() {
    var checkboxes = $("input[type='checkbox']"),
    submitButt = $("button[type='submit']");

    checkboxes.click(function() {
        submitButt.attr("disabled", !checkboxes.is(":checked"));
    });
  });
</script>
@endsection