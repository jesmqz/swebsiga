@extends('layouts.app')

@section('content')
<div class="container cit-content">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Actualizar solicitud</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('solicitud.update', ['id_solicitud' => $solicitud->id ]) }}">
                        @csrf
                        @method('PUT')
                        <input type="hidden" value="1" name="prueba">
                        <div class="form-group row">
                            <label for="id" class="col-md-4 col-form-label text-md-right">{{ __('id') }}</label>
                            <div class="col-md-6">
                                <input id="id" class="form-control{{ $errors->has('id') ? ' is-invalid' : '' }}" name="id" value="{{ $solicitud->id }}" required readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('username') }}</label>
                            <div class="col-md-6">
                                <input id="username" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" value="{{ $solicitud->username }}" name="username" required readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="codigo_asignatura" class="col-md-4 col-form-label text-md-right">{{ __('codigo_asignatura') }}</label>
                            <div class="col-md-6">
                                <input id="codigo_asignatura" class="form-control{{ $errors->has('codigo_asignatura') ? ' is-invalid' : '' }}" value="{{ $solicitud->codigo_asignatura }}" name="codigo_asignatura" required readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="modalidad" class="col-md-4 col-form-label text-md-right">{{ __('modalidad') }}</label>
                            <div class="col-md-6">
                                <input 
                                    id="modalidad"
                                    class="form-control{{ $errors->has('modalidad') ? ' is-invalid' : '' }}"
                                    value="{{ $solicitud->modalidad }}"
                                    name="modalidad" required readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="facultad" class="col-md-4 col-form-label text-md-right">{{ __('facultad') }}</label>
                            <div class="col-md-6">
                                <input id="facultad" class="form-control{{ $errors->has('facultad') ? ' is-invalid' : '' }}" value="{{ $solicitud->facultad }}" name="facultad" required readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="departamento" class="col-md-4 col-form-label text-md-right">{{ __('departamento') }}</label>
                            <div class="col-md-6">
                                <input id="departamento" class="form-control{{ $errors->has('departamento') ? ' is-invalid' : '' }}" value="{{ $solicitud->departamento }}" name="departamento" required readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="state" class="col-md-4 col-form-label text-md-right">{{ __('estado') }}</label>
                            <div class="col-md-6">
                                <select  id="state" class="form-control" name="state">
                                    <option @if ($solicitud->state == '0') selected @endif value="0">Sin procesar</option>
                                    <option @if ($solicitud->state == '1') selected @endif value="1">Espacio creado</option>
                                    <option @if ($solicitud->state == '2') selected @endif value="2">Matriculado</option>
                                    <option @if ($solicitud->state == '3') selected @endif value="3">Actualizando</option>
                                    <option @if ($solicitud->state == '4') selected @endif value="4">Validado</option>
                                    <option @if ($solicitud->state == '5') selected @endif value="5">Pausado</option>
                                    <option @if ($solicitud->state == '6') selected @endif value="6">Error</option>
                                    <option @if ($solicitud->state == '7') selected @endif value="7">Sin actualizar</option>
                                    <option @if ($solicitud->state == '8') selected @endif value="8">Desmatricular</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">Actualizar</button>
                                <button class="btn btn-primary">Cancelar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection