<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>UAO Virtual - SGA</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <!-- FontAwesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
</head>
<body>
    <nav class="cit-header">
            <div class="container cit-container-nav">
                <a href="{{ url('/') }}">
                    <img class="cit-header-logo" src="{{ asset('img/logo.png') }}">    
                </a> 
                <ul class="nav nav-tabs">
                <li class="nav-item dropdown">
                
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <img class="cit-nav-menu-img" src="{{ asset('img/menu.png') }}">
                </a>
                <div class="dropdown-menu dropdown-menu-right cit-nav-menu">
                @guest
                <a class="dropdown-item" href="{{ route('login') }}">{{ __('Iniciar sesión') }}</a>
                @else
                <h4 id="navbarDropdown" class="cit-nav-menu-user-name">
                {{ Auth::user()->name }} <span class="caret"></span>
                </h4>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('administration')}}">Administración</a>
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                {{ __('Cerrar sesión') }}</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
                </form>
                @endguest
                        </div>
                </li>
                </ul>   
            </div>    
    </nav>
    <div id="app">
        @yield('content')
    </div>
</body>
</html>
