@extends('layouts.app')

@section('content')
    <div class="container cit-content">
        <div class="row justify-content-center no-gutters">
            <div class="col-md-10">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent">
                  <li class="breadcrumb-item"><a href={{ route('home') }}>Home</a></li>
                  <li class="breadcrumb-item"><a href={{ route('administration') }}>Administración</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Usuarios</li>
                </ol>
              </nav>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center" style="height:50px;">
                <div class="col-md-8">
                    @if(Session::has('message'))
                        <flashmessage-component message="{{ Session::get('message') }}"></flashmessage-component>
                    @else
                        <flashmessage-component message="¡Lista de usuarios ok!"></flashmessage-component>
                    @endif
                </div>
        </div>
    </div>
    <div class="container">
        <div class="row align-items-center justify-content-center" style="height:100px;">
            <div class="col-md-auto">
                <form class="form-inline">
                    <div class="form-group mx-sm-3 mb-2">
                        <label for="inputPassword2" class="sr-only">Username</label>
                        <input type="text" class="form-control" id="inputPassword2" placeholder="Username">
                    </div>
                    <button type="submit" class="btn btn-primary mb-2">Buscar</button>
                </form>
                
            </div>
        </div>
        
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table">
                      <thead>
                          <tr>
                              <th scope="col" style="width: 2%">#</th>
                              <th scope="col" style="width: 30%">Nombre y Apellido</th>
                              <th scope="col" style="width: 20%">Email</th>
                              <th scope="col" style="width: 20%">Creado</th>
                              <th scope="col" style="width: 20%">Último acceso</th>
                              <th scope="col" colspan="2" style="width: 8%">Acciones</th>
                          </tr>
                      </thead>
                      <tbody>
                          @php
                              $i=1;
                          @endphp
                          @foreach ($users as $user)
                              <tr>
                                  <th scope="row">{{ $i }}</th>
                                  <td>{{ $user->name }}</td>
                                  <td>{{ $user->email }}</td>
                                  <td>{{ $user->created_at }}</td>
                                  <td>{{ $user->updated_at }}</td>
                                  <td class="actions"><a href={{ route('editUser', ['email' => $user->email])}} class="icon"><i class="fas fa-edit"></i></a></td>
                                  <td class="actions"><button-delete-component r={{ route('deleteUser', ['email' => $user->email]) }} e={{ $user->email }}></button-delete-component></td>
                              </tr>
                              @php
                                  $i++;
                              @endphp
                          @endforeach
                      </tbody>
                  </table>
                </div>
            </div>
        </div>
        <deletemodal-component urlin="http:\\ibom.com"></deletemodal-component>
    </div>

    <div class="container" style="height: 200px;">
        <div class="row align-items-center justify-content-center" style="height: 150px;">
            <div class="col-md-auto">
                <a href="{{ route('createUser')}}" class="btn btn-primary btn-lg" tabindex="-1" role="button" aria-disabled="true">Nuevo</a>
            </div>
        </div>
    </div>
@endsection
