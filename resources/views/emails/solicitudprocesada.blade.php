@component('mail::message')
![UAO Virtual][logo]
[logo]: {{ asset('/public/img/logo_email.png')}} "Logo"

<h2>Estimado docente,</h2>

<p>Le informamos que el trámite de activación y matrícula de estudiantes de su curso <b>{{ $solicitud['shortname'] }}</b> ha sido realizado.</p>
<p>A partir de este momento podrá ingresar al curso en la plataforma. Por favor verificar que todo esté bien, recuerde modificar las fechas de las actividades.</p>

@component('mail::button', ['url' => $solicitud['linkToCampus'] ])
¡Ir al curso!
@endcomponent

<p>Solicitud # <b>{{ $solicitud['id'] }}</b></p>
<p>Usuario docente: <b>{{ $solicitud['username'] }}</b></p>

Atentamente,<br>
Unidad de Campus Virtual
@endcomponent
