@extends('layouts.app')
@section('content')
    @if(Auth::check())
        {{-- breadcrumb --}}
        <div class="container cit-content">
            <div class="row justify-content-center no-gutters">
                <div class="col-md-10">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb bg-transparent">
                    <li class="breadcrumb-item"><a href={{ route('home') }} class="text-dark">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Reportes</li>
                    </ol>
                </nav>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-9 offset-md-3">
                    <h3 class="font-weight-bold">Período: {{ $current_period }}</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-12 p-4">
                    <div class="nav flex-column nav-pills menu-report" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link active link-menu-report" id="v-pills-reporte1-tab" data-toggle="pill" href="#v-pills-reporte1" role="tab" aria-controls="v-pills-reporte1" aria-selected="true" style="font-size: 14px;">Balance general</a>
                        <a class="nav-link link-menu-report" id="v-pills-reporte2-tab" data-toggle="pill" href="#v-pills-reporte2" role="tab" aria-controls="v-pills-reporte2" aria-selected="false">Comparativo oferta por modalidad</a>
                        <a class="nav-link link-menu-report" id="v-pills-reporte3-tab" data-toggle="pill" href="#v-pills-reporte3" role="tab" aria-controls="v-pills-reporte3" aria-selected="false">Comparativo oferta E-learning por facultad</a>
                        <a class="nav-link link-menu-report" id="v-pills-reporte4-tab" data-toggle="pill" href="#v-pills-reporte4" role="tab" aria-controls="v-pills-reporte4" aria-selected="false">Detalle oferta cursos E-learning</a>
                        <a class="nav-link link-menu-report" id="v-pills-reporte5-tab" data-toggle="pill" href="#v-pills-reporte5" role="tab" aria-controls="v-pills-reporte5" aria-selected="false">Comparativo oferta B-learning por facultad - Por nivel de formación</a>
                        <a class="nav-link link-menu-report" id="v-pills-reporte6-tab" data-toggle="pill" href="#v-pills-reporte6" role="tab" aria-controls="v-pills-reporte6" aria-selected="false">Comparativo oferta B-learning - Por facultad</a>
                        <a class="nav-link link-menu-report" id="v-pills-reporte7-tab" data-toggle="pill" href="#v-pills-reporte7" role="tab" aria-controls="v-pills-reporte7" aria-selected="false">Detalle oferta de cursos B-learning</a>
                        <a class="nav-link link-menu-report" id="v-pills-reporte8-tab" data-toggle="pill" href="#v-pills-reporte8" role="tab" aria-controls="v-pills-reporte8" aria-selected="false">Cursos B-learning por facultad</a>
                        <a class="nav-link link-menu-report" id="v-pills-reporte9-tab" data-toggle="pill" href="#v-pills-reporte9" role="tab" aria-controls="v-pills-reporte9" aria-selected="false">Estudiantes B-learning por facultad</a>
                        <a class="nav-link link-menu-report" id="v-pills-reporte10-tab" data-toggle="pill" href="#v-pills-reporte10" role="tab" aria-controls="v-pills-reporte10" aria-selected="false">Cursos B-learning por facultades y departamento</a>
                        <a class="nav-link link-menu-report" id="v-pills-reporte11-tab" data-toggle="pill" href="#v-pills-reporte11" role="tab" aria-controls="v-pills-reporte11" aria-selected="false">Estudiantes E-learning por programa</a>
                        <a class="nav-link link-menu-report" id="v-pills-reporte12-tab" data-toggle="pill" href="#v-pills-reporte12" role="tab" aria-controls="v-pills-reporte12" aria-selected="false">Estudiantes B-learning por programa</a>
                        <a class="nav-link link-menu-report" id="v-pills-reporte13-tab" data-toggle="pill" href="#v-pills-reporte13" role="tab" aria-controls="v-pills-reporte13" aria-selected="false">Estudiantes por ciclo</a>
                        <a class="nav-link link-menu-report" id="v-pills-reporte14-tab" data-toggle="pill" href="#v-pills-reporte14" role="tab" aria-controls="v-pills-reporte14" aria-selected="false">Facultades y departamentos</a>
                        <a class="nav-link link-menu-report" id="v-pills-reporte15-tab" data-toggle="pill" href="#v-pills-reporte15" role="tab" aria-controls="v-pills-reporte15" aria-selected="false">Departamentos y cursos</a>
                    </div>
                </div>
                <div class="col-md-9 col-12 p-4">
                    <div class="tab-content" id="v-pills-tabContent">
                        <!-- balance general -->
                        <div class="tab-pane fade show active" id="v-pills-reporte1" role="tabpanel" aria-labelledby="v-pills-reporte1-tab">
                            <table class="table" style="font-family: Roboto, sans-serif">
                                <thead class="thead-dark">
                                <tr>
                                    <th>Modalidad</th>
                                    <th>Cursos</th>
                                    <th>Estudiantes</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="table-light">E-learning</td>
                                        <td class="table-light">{{ $balance_general['e-learning']['total_courses'] }}</td>
                                        <td class="table-light">{{ $balance_general['e-learning']['total_students'] }}</td>
                                    </tr>
                                    <tr>
                                        <td class="table-light">B-learning (SIGA)</td>
                                        <td class="table-light">{{ $balance_general['b-learning']['total_courses'] }}</td>
                                        <td class="table-light">{{ $balance_general['b-learning']['total_students'] }}</td>
                                    </tr>
                                    <tr>
                                        <td class="table-light">Capacitación</td>
                                        <td class="table-light">none</td>
                                        <td class="table-light">none</td>
                                    </tr>
                                    <tr>
                                        <td class="table-light">Evaluación Institucional</td>
                                        <td class="table-light">none</td>
                                        <td class="table-light">none</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--  comparativo oferta por modalidad  -->
                        <div class="tab-pane fade" id="v-pills-reporte2" role="tabpanel" aria-labelledby="v-pills-reporte2-tab">
                                <table class="table" style="font-family: Roboto, sans-serif">
                                        <thead class="thead-dark">
                                            <tr style="text-align: center;">
                                                <th rowspan="2">Modalidad</th>
                                                <th colspan="2">{{ $comparative_offer_by_mode[3]['period'] }}</th>
                                                <th colspan="2">{{ $comparative_offer_by_mode[2]['period'] }}</th>
                                                <th colspan="2">{{ $comparative_offer_by_mode[1]['period'] }}</th>
                                                <th colspan="2">{{ $current_period }}</th>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <th>Cursos</th>
                                                <th>Estudiantes</th>
                                                <th>Cursos</th>
                                                <th>Estudiantes</th>
                                                <th>Cursos</th>
                                                <th>Estudiantes</th>
                                                <th>Cursos</th>
                                                <th>Estudiantes</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr style="text-align: center;">
                                                <td class="table-light">E-learning</td>
                                                <td class="table-light">{{ $comparative_offer_by_mode[3]['total_mode']['e-learning']['total_courses'] }}</td>
                                                <td class="table-light">{{ $comparative_offer_by_mode[3]['total_mode']['e-learning']['total_students'] }}</td>
                                                <td class="table-light">{{ $comparative_offer_by_mode[2]['total_mode']['e-learning']['total_courses'] }}</td>
                                                <td class="table-light">{{ $comparative_offer_by_mode[2]['total_mode']['e-learning']['total_students'] }}</td>
                                                <td class="table-light">{{ $comparative_offer_by_mode[1]['total_mode']['e-learning']['total_courses'] }}</td>
                                                <td class="table-light">{{ $comparative_offer_by_mode[1]['total_mode']['e-learning']['total_students'] }}</td>
                                                <td class="table-light">{{ $balance_general['e-learning']['total_courses'] }}</td>
                                                <td class="table-light">{{ $balance_general['e-learning']['total_students'] }}</td>
                                            </tr>
                                            <tr style="text-align: center;">
                                                <td class="table-light">B-learning</td>
                                                <td class="table-light">{{ $comparative_offer_by_mode[3]['total_mode']['b-learning']['total_courses'] }}</td>
                                                <td class="table-light">{{ $comparative_offer_by_mode[3]['total_mode']['b-learning']['total_students'] }}</td>
                                                <td class="table-light">{{ $comparative_offer_by_mode[2]['total_mode']['b-learning']['total_courses'] }}</td>
                                                <td class="table-light">{{ $comparative_offer_by_mode[2]['total_mode']['b-learning']['total_students'] }}</td>
                                                <td class="table-light">{{ $comparative_offer_by_mode[1]['total_mode']['b-learning']['total_courses'] }}</td>
                                                <td class="table-light">{{ $comparative_offer_by_mode[1]['total_mode']['b-learning']['total_students'] }}</td>
                                                <td class="table-light">{{ $balance_general['b-learning']['total_courses'] }}</td>
                                                <td class="table-light">{{ $balance_general['b-learning']['total_students'] }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                        </div>
                        <!--  comparativo oferta E-learning por facultad  -->
                        <div class="tab-pane fade" id="v-pills-reporte3" role="tabpanel" aria-labelledby="v-pills-reporte3-tab">
                            <table class="table" style="font-family: Roboto, sans-serif">
                                    <thead class="thead-dark">
                                        <tr style="text-align: center;">
                                            <th rowspan="2">Facultad</th>
                                            <th colspan="2">{{ $comparative_offer_by_faculty[3]['period'] }}</th>
                                            <th colspan="2">{{ $comparative_offer_by_faculty[2]['period'] }}</th>
                                            <th colspan="2">{{ $comparative_offer_by_faculty[1]['period'] }}</th>
                                            <th colspan="2">{{ $current_period }}</th>
                                        </tr>
                                        <tr style="text-align: center;">
                                            <th>Cursos</th>
                                            <th>Estudiantes</th>
                                            <th>Cursos</th>
                                            <th>Estudiantes</th>
                                            <th>Cursos</th>
                                            <th>Estudiantes</th>
                                            <th>Cursos</th>
                                            <th>Estudiantes</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {{-- iterations by the faculties --}}
                                        @foreach ($faculties_analize as $faculty)
                                            <tr style="text-align: center;">
                                                <td class="table-light" style="text-align: left">{{ $faculty }}</td>
                                                {{-- period #1 --}}
                                                @if (array_key_exists($faculty, $comparative_offer_by_faculty[3]['total_courses_elearning']))
                                                    <td class="table-light">{{ $comparative_offer_by_faculty[3]['total_courses_elearning'][$faculty] }}</td>
                                                    <td class="table-light">{{ $comparative_offer_by_faculty[3]['total_students_elearning'][$faculty] }}</td>
                                                @else
                                                    <td class="table-light">0</td>
                                                    <td class="table-light">0</td>
                                                @endif


                                                {{-- period #2 --}}
                                                @if (array_key_exists($faculty, $comparative_offer_by_faculty[2]['total_courses_elearning']))
                                                    <td class="table-light">{{ $comparative_offer_by_faculty[2]['total_courses_elearning'][$faculty] }}</td>
                                                    <td class="table-light">{{ $comparative_offer_by_faculty[2]['total_students_elearning'][$faculty] }}</td>
                                                @else
                                                    <td class="table-light">0</td>
                                                    <td class="table-light">0</td>
                                                @endif
                                                {{-- period #3 --}}
                                                @if (array_key_exists($faculty, $comparative_offer_by_faculty[1]['total_courses_elearning']))
                                                    <td class="table-light">{{ $comparative_offer_by_faculty[1]['total_courses_elearning'][$faculty] }}</td>
                                                    <td class="table-light">{{ $comparative_offer_by_faculty[1]['total_students_elearning'][$faculty] }}</td>
                                                @else
                                                    <td class="table-light">0</td>
                                                    <td class="table-light">0</td>
                                                @endif
                                                {{-- period #4 --}}
                                                @if (array_key_exists($faculty, $comparative_offer_by_faculty[0]['total_courses_elearning']))
                                                    <td class="table-light">{{ $comparative_offer_by_faculty[0]['total_courses_elearning'][$faculty] }}</td>
                                                    <td class="table-light">{{ $comparative_offer_by_faculty[0]['total_students_elearning'][$faculty] }}</td>
                                                @else
                                                    <td class="table-light">0</td>
                                                    <td class="table-light">0</td>
                                                @endif
                                            </tr>
                                        @endforeach
                                        <tr class="font-weight-bold" style="text-align: center;">
                                                <td scope="row">Total</td>
                                                <td class="table-light">{{ $total_comparative_offer[3]['total_courses_elearning'] }}</td>
                                                <td class="table-light">{{ $total_comparative_offer[3]['total_students_elearning'] }}</td>
                                                <td class="table-light">{{ $total_comparative_offer[2]['total_courses_elearning'] }}</td>
                                                <td class="table-light">{{ $total_comparative_offer[2]['total_students_elearning'] }}</td>
                                                <td class="table-light">{{ $total_comparative_offer[1]['total_courses_elearning'] }}</td>
                                                <td class="table-light">{{ $total_comparative_offer[1]['total_students_elearning'] }}</td>
                                                <td class="table-light">{{ $total_comparative_offer[0]['total_courses_elearning'] }}</td>
                                                <td class="table-light">{{ $total_comparative_offer[0]['total_students_elearning'] }}</td>
                                            </tr>
                                    </tbody>
                                </table>
                        </div>
                        <!-- Detalle oferta cursos E-learning -->
                        <div class="tab-pane fade" id="v-pills-reporte4" role="tabpanel" aria-labelledby="v-pills-reporte4-tab">
                            <table class="table" style="font-family: Roboto, sans-serif">
                                <thead class="thead-dark">
                                    <tr style="text-align: center;">
                                        <th>Nivel</th>
                                        <th>Facultad</th>
                                        <th>Departamento</th>
                                        <th>Asignatura</th>
                                        <th>Matriculados</th>
                                        <th>% retención</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- iterar niveles -->
                                    @foreach ($detail_offers_elearning as $n)
                                        <!-- cada nivel tiene su n rows -->
                                        <tr>
                                            <!-- celda del nombre de nivel -->
                                            <td rowspan={{ $n["total_subjects"] }} class="table-light">{{ $n["name"] }} </td>
                                            <!-- iterar por las facultades -->
                                            @foreach ($n["faculties"] as $faculty)
                                                <!-- celda del nombre de facultad -->
                                                @if ($loop->first)
                                                    <td rowspan={{ $faculty["total_subjects"]}} style="text-align: left;" class="table-light">{{ $faculty["name"] }}</td>
                                                @else
                                                    <tr>
                                                    <td rowspan={{ $faculty["total_subjects"]}} style="text-align: left;" class="table-light">{{ $faculty["name"] }}</td>
                                                @endif
                                                <!-- iterar por los departamentos -->
                                                @foreach ($faculty["departments"] as $department)
                                                    <!-- celda nombre departamento-->
                                                    @if ($loop->first)
                                                        <td rowspan={{ $department["total_subjects"] }} style="text-align: left;" class="table-light">{{ $department["name"] }}</td>
                                                    @else
                                                        <tr>
                                                        <td rowspan={{ $department["total_subjects"] }} style="text-align: left;" class="table-light">{{ $department["name"] }}</td>
                                                    @endif
                                                    
                                                    @foreach ($department["subjects"] as $subject)
                                                        @if ($loop->first)
                                                            <td class="table-light" style="text-align: center;">{{ $subject["code"] }}</td>
                                                            <td class="table-light" style="text-align: center;">{{ $subject["enrolled"] }}</td>
                                                            <td class="table-light" style="text-align: center;">{{ $subject["retention"] }}</td>
                                                            </tr>
                                                        @else
                                                            <tr style="text-align: center;">
                                                            <td class="table-light">{{ $subject["code"] }}</td>
                                                            <td class="table-light">{{ $subject["enrolled"] }}</td>
                                                            <td class="table-light">{{ $subject["retention"] }}</td>
                                                            </tr>
                                                        @endif
                                                    @endforeach {{-- subjects --}}
                                                @endforeach {{-- departments --}}
                                            @endforeach {{-- faculties --}}
                                        @endforeach {{-- levels --}}
                                </tbody>
                            </table>
                        </div>
                        <!-- Comparativo oferta B-learning por nivel de formación -->
                        <div class="tab-pane fade" id="v-pills-reporte5" role="tabpanel" aria-labelledby="v-pills-reporte5-tab">
                            <table class="table" style="text-align: center; font-family: Roboto, sans-serif;">
                                <thead class="thead-dark">
                                    <tr>
                                        <th rowspan="2">Nivel</th>
                                        <th colspan="2">2017-01</th>
                                        <th colspan="2">2017-03</th>
                                        <th colspan="2">2018-01</th>
                                        <th colspan="2">2018-03</th>
                                    </tr>
                                    <tr>
                                        <th>Cursos</th>
                                        <th>Estudiantes</th>
                                        <th>Cursos</th>
                                        <th>Estudiantes</th>
                                        <th>Cursos</th>
                                        <th>Estudiantes</th>
                                        <th>Cursos</th>
                                        <th>Estudiantes</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td scope="row" style="text-align: left;">Pregrado Profesional</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                    </tr>
                                    <tr>
                                        <td scope="row" style="text-align: left;">Pregrado tecnológico</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                    </tr>
                                    <tr>
                                        <td scope="row" style="text-align: left;">Posgrado</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                    </tr>
                                    <tr class="font-weight-bold">
                                        <td scope="row" style="text-align: left;">Total</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                        <td class="table-light">?</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- Comparativo oferta B-learning por facultad -->
                        <div class="tab-pane fade" id="v-pills-reporte6" role="tabpanel" aria-labelledby="v-pills-reporte6-tab">
                            <table class="table" style="font-family: Roboto, sans-serif">
                                    <thead class="thead-dark">
                                        <tr style="text-align: center;">
                                            <th rowspan="2">Facultad</th>
                                            <th colspan="2">{{ $comparative_offer_by_faculty[3]['period'] }}</th>
                                            <th colspan="2">{{ $comparative_offer_by_faculty[2]['period'] }}</th>
                                            <th colspan="2">{{ $comparative_offer_by_faculty[1]['period'] }}</th>
                                            <th colspan="2">{{ $current_period }}</th>
                                        </tr>
                                        <tr style="text-align: center;">
                                            <th>Cursos</th>
                                            <th>Estudiantes</th>
                                            <th>Cursos</th>
                                            <th>Estudiantes</th>
                                            <th>Cursos</th>
                                            <th>Estudiantes</th>
                                            <th>Cursos</th>
                                            <th>Estudiantes</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {{-- iterations by the faculties --}}
                                        @foreach ($faculties_analize as $faculty)
                                            <tr>
                                                <td class="table-light" style="text-align: left;">{{ $faculty }}</td>
                                                {{-- period #1 --}}
                                                @if (array_key_exists($faculty, $comparative_offer_by_faculty[3]['total_courses_blearning']))
                                                    <td class="table-light">{{ $comparative_offer_by_faculty[3]['total_courses_blearning'][$faculty] }}</td>
                                                    <td class="table-light">{{ $comparative_offer_by_faculty[3]['total_students_blearning'][$faculty] }}</td>
                                                @else
                                                    <td class="table-light">0</td>
                                                    <td class="table-light">0</td>
                                                @endif
                                                {{-- period #2 --}}
                                                @if (array_key_exists($faculty, $comparative_offer_by_faculty[2]['total_courses_blearning']))
                                                    <td class="table-light">{{ $comparative_offer_by_faculty[2]['total_courses_blearning'][$faculty] }}</td>
                                                    <td class="table-light">{{ $comparative_offer_by_faculty[2]['total_students_blearning'][$faculty] }}</td>
                                                @else
                                                    <td class="table-light">0</td>
                                                    <td class="table-light">0</td>
                                                @endif
                                                {{-- period #3 --}}
                                                @if (array_key_exists($faculty, $comparative_offer_by_faculty[1]['total_courses_blearning']))
                                                    <td class="table-light">{{ $comparative_offer_by_faculty[1]['total_courses_blearning'][$faculty] }}</td>
                                                    <td class="table-light">{{ $comparative_offer_by_faculty[1]['total_students_blearning'][$faculty] }}</td>
                                                @else
                                                    <td class="table-light">0</td>
                                                    <td class="table-light">0</td>
                                                @endif
                                                {{-- period #4 --}}
                                                @if (array_key_exists($faculty, $comparative_offer_by_faculty[0]['total_courses_blearning']))
                                                    <td class="table-light">{{ $comparative_offer_by_faculty[0]['total_courses_blearning'][$faculty] }}</td>
                                                    <td class="table-light">{{ $comparative_offer_by_faculty[0]['total_students_blearning'][$faculty] }}</td>
                                                @else
                                                    <td class="table-light">0</td>
                                                    <td class="table-light">0</td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                        </div>
                        <!-- detail blearning courses offers -->
                        <div class="tab-pane fade" id="v-pills-reporte7" role="tabpanel" aria-labelledby="v-pills-reporte7-tab">
                            <table class="table" style="font-family: Roboto, sans-serif">
                                    <thead class="thead-dark">
                                        <tr style="text-align: center;">
                                            <th>Nivel</th>
                                            <th>Facultad</th>
                                            <th>Departamento</th>
                                            <th># asignaturas</th>
                                            <th>Matriculados</th>
                                            <!-- <th>% retención</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!-- it iterate levels -->
                                        @foreach ($detail_offers_blearning as $n)
                                            <!-- each level has n rows  -->
                                            <tr>
                                                <!-- level name cell -->
                                                <td rowspan={{ $n["total_subjects"] }} class="table-light" style="text-align: left;">{{ $n["name"] }} </td>
                                                <!-- it iterate by faculties  -->
                                                @foreach ($n["faculties"] as $faculty)
                                                    <!-- faculty name cell -->
                                                    @if ($loop->first)
                                                        <td rowspan={{ $faculty["total_departments"]}} class="table-light" style="text-align: left;">{{ $faculty["name"] }}</td>
                                                    @else
                                                        <tr>
                                                        <td rowspan={{ $faculty["total_departments"]}} style="text-align: left;" class="table-light">{{ $faculty["name"] }}</td>
                                                    @endif
                                                    <!-- it iterate by departaments -->
                                                    @foreach ($faculty["departments"] as $department)
                                                        <!-- departament name cell -->
                                                        @if ($loop->first)
                                                            {{-- <td rowspan={{ $department["total_subjects"] }}>{{ $department["name"] }}</td> --}}
                                                            <td class="table-light" style="text-align: left;">{{ $department["name"] }}</td>
                                                            <td class="table-light" style="text-align: center;">{{ $department["total_subjects"] }}</td>
                                                            <td class="table-light" style="text-align: center;">{{ $department["total_students"] }}</td>
                                                            </tr>
                                                        @else
                                                            <tr>
                                                            {{-- <td rowspan={{ $department["total_subjects"] }}>{{ $department["name"] }}</td> --}}
                                                            <td class="table-light" style="text-align: left;">{{ $department["name"] }}</td>
                                                            <td class="table-light" style="text-align: center;">{{ $department["total_subjects"] }}</td>
                                                            <td class="table-light" style="text-align: center;">{{ $department["total_students"] }}</td>
                                                            </tr>
                                                        @endif
                                                        {{--
                                                        @foreach ($department["subjects"] as $subject)
                                                            @if ($loop->first)
                                                                <td class="table-light">{{ $subject["code"] }}</td>
                                                                <td class="table-light">{{ $subject["enrolled"] }}</td>
                                                                <td class="table-light">{{ $subject["retention"] }}</td>
                                                                </tr>
                                                            @else
                                                                <tr style="text-align: center;">
                                                                <td class="table-light">{{ $subject["code"] }}</td>
                                                                <td class="table-light">{{ $subject["enrolled"] }}</td>
                                                                <td class="table-light">{{ $subject["retention"] }}</td>
                                                                </tr>
                                                            @endif
                                                        @endforeach 
                                                        --}}
                                                    @endforeach {{-- departments --}}
                                                @endforeach {{-- faculties --}}
                                            @endforeach {{-- levels --}}
                                    </tbody>
                                </table>
                        </div>
                        <!-- courses b-learning by faculty -->
                        <div class="tab-pane fade" id="v-pills-reporte8" role="tabpanel" aria-labelledby="v-pills-reporte8-tab">
                            <table class="table" style="font-family: Roboto, sans-serif">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th rowspan="2">Facultad</th>
                                            <th colspan="2" style="text-align: center;">Total cursos</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($faculties as $faculty)
                                            <tr>
                                                <td scope="row" class="table-light">{{ $faculty->facultad }}</td>
                                                <td style="text-align: center;" class="table-light">{{ $comparative_offer_by_faculty[0]['total_courses_blearning'][$faculty->facultad] }}</td>
                                            </tr>                                    
                                        @endforeach
                                        <tr class="font-weight-bold">
                                            <td scope="row" class="table-light">Total</td>
                                            <td style="text-align: center;" class="table-light">{{ $comparative_offer_by_mode[0]['total_mode']['b-learning']['total_courses']}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                        </div>
                        <!-- students blearning by faculty -->
                        <div class="tab-pane fade" id="v-pills-reporte9" role="tabpanel" aria-labelledby="v-pills-reporte9-tab">
                            <table class="table" style="font-family: Roboto, sans-serif">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th rowspan="2">Facultad</th>
                                            <th colspan="2" style="text-align: center;">Total estudiantes</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($faculties as $faculty)
                                            <tr>
                                                <td scope="row" class="table-light">{{ $faculty->facultad }}</td>
                                                <td style="text-align: center;" class="table-light">{{ $comparative_offer_by_faculty[0]['total_students_blearning'][$faculty->facultad] }}</td>
                                            </tr>                                    
                                        @endforeach
                                        <tr class="font-weight-bold">
                                            <td scope="row" class="table-light">Total</td>
                                            <td style="text-align: center;" class="table-light">{{ $comparative_offer_by_mode[0]['total_mode']['b-learning']['total_students']}}</td>
                                        </tr>
    
                                    </tbody>
                                </table> 
                        </div>
                        <!-- courses belearning by departments and faculties -->
                        <div class="tab-pane fade" id="v-pills-reporte10" role="tabpanel" aria-labelledby="v-pills-reporte10-tab">
                            <courses-tables
                                v-bind:faculties="{{ $faculties_implementation }}">
                            </courses-tables>
                        </div>
                        <div class="tab-pane fade" id="v-pills-reporte11" role="tabpanel" aria-labelledby="v-pills-reporte11-tab">
                            <table class="table" style="font-family: Roboto, sans-serif">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th>Programas</th>
                                        <th># estudiantes</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($total_students_e_by_programa as $programa => $total)
                                        <tr>
                                            <td class="table-light">{{ $programas_asoc[$programa] }}</td>
                                            <td class="table-light">{{ $total }}</td>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <td class="table-light">Total:</td>
                                            <td class="table-light">{{ $total_students_e }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                        </div>
                        <div class="tab-pane fade" id="v-pills-reporte12" role="tabpanel" aria-labelledby="v-pills-reporte12-tab">
                            <table class="table" style="font-family: Roboto, sans-serif">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th>Programas</th>
                                        <th># estudiantes</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($total_students_b_by_programa as $programa => $total)
                                        <tr>
                                            <td class="table-light">{{ $programas_asoc[$programa] }}</td>
                                            <td class="table-light">{{ $total }}</td>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <td class="table-light">Total:</td>
                                            <td class="table-light">{{ $total_students_b }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                        </div>
                        <div class="tab-pane fade" id="v-pills-reporte13" role="tabpanel" aria-labelledby="v-pills-reporte13-tab">
                            <table class="table" style="font-family: Roboto, sans-serif">
                                <thead class="thead-dark">
                                <tr>
                                    <th>Ciclos</th>
                                    <th># estudiantes</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($total_students_by_ciclo as $ciclo => $total)
                                    <tr>
                                        <td class="table-light">{{ $ciclo }}</td>
                                        <td class="table-light">{{ $total }}</td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td class="table-light">Total:</td>
                                        <td class="table-light">{{ $total_students_ciclo }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="v-pills-reporte14" role="tabpanel" aria-labelledby="v-pills-reporte14-tab">
                            <div class="alert alert-secondary" role="alert">
                                Details faculties his departments and his total subjects 
                            </div>
                            <ul class="list-group">
                                @foreach ($detail_offers_elearning[0]['faculties'] as $faculty)
                                    @foreach ($faculty['departments'] as $department)
                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                            {{ $faculty['name'] }} - {{ $department['name'] }}
                                            <span class="badge badge-primary badge-pill">{{ $department['total_subjects'] }}</span>
                                        </li>
                                    @endforeach
                                @endforeach
                            </ul>
                        </div>
                        <div class="tab-pane fade" id="v-pills-reporte15" role="tabpanel" aria-labelledby="v-pills-reporte15-tab">
                            <table class="table" style="font-family: Roboto, sans-serif">
                                <thead class="thead-dark">
                                <tr>
                                    <th>Departamentos</th>
                                    <th>Cursos</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($departments_courses as $dpto => $courses)
                                    <tr>
                                        <td class="table-light">{{ $dpto }}</td>
                                        <td class="table-light">{{ $courses }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> <!-- row -->
        </div>
    @else
    <div class="alert alert-warning" role="alert">
        ¡No estas autenticado, ir ha 
        <a href="{{ route('login') }}">login</a> 
    </div>
    @endif
@endsection