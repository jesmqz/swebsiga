@extends('layouts.app')
@section('content')
    <div class="container-fluid cit-content">
        <div class="row">
            <div class="col">
                <div class="alert alert-Here will be a table!" role="alert">
                    faculties analize
                    array: {{ var_dump($faculties_analize) }}
                </div>

                <div class="alert alert-Here will be a table!" role="alert">
                    total
                    array: {{ var_dump($total) }}
                </div>

                {{-- 
                <table class="table">
                    <thead class="thead-dark">
                        <tr style="text-align: center;">
                            <th>Nivel</th>
                            <th>Facultad</th>
                            <th>Departamento</th>
                            <th>Asignatura</th>
                            <th>Matriculados</th>
                            <th>% retención</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- iterar niveles -->
                        @foreach ($nivel as $n)
                            <!-- cada nivel tiene su n rows -->
                            <tr style="text-align: center;">
                                <!-- celda del nombre de nivel -->
                                <td rowspan={{ $n["total_asignaturas"] }}>{{ $n["nombre"] }} </td>
                                <!-- iterar por las facultades -->
                                @foreach ($n["facultades"] as $facultad)
                                    <!-- celda del nombre de facultad -->
                                    @if ($loop->first)
                                        <td rowspan={{ $facultad["total_asignaturas"]}}>{{ $facultad["nombre"] }}</td>
                                    @else
                                        <tr style="text-align: center;">
                                        <td rowspan={{ $facultad["total_asignaturas"]}}>{{ $facultad["nombre"] }}</td>
                                    @endif
                                    <!-- iterar por los departamentos -->
                                    @foreach ($facultad["departamentos"] as $departamento)
                                        <!-- celda nombre departamento-->
                                        @if ($loop->first)
                                            <td rowspan={{ $departamento["total_asignaturas"] }}>{{ $departamento["nombre"] }}</td>
                                        @else
                                            <tr style="text-align: center;">
                                            <td rowspan={{ $departamento["total_asignaturas"] }}>{{ $departamento["nombre"] }}</td>
                                        @endif
                                        
                                        @foreach ($departamento["asignaturas"] as $asignatura)
                                            @if ($loop->first)
                                                <td>{{ $asignatura["nombre"] }}</td>
                                                <td>{{ $asignatura["matriculados"] }}</td>
                                                <td>{{ $asignatura["retencion"] }}</td>
                                                </tr>
                                            @else
                                                <tr style="text-align: center;">
                                                <td>{{ $asignatura["nombre"] }}</td>
                                                <td>{{ $asignatura["matriculados"] }}</td>
                                                <td>{{ $asignatura["retencion"] }}</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    @endforeach
                                @endforeach
                         @endforeach
                    </tbody>
                </table>

                
                <ul class="list-group">
                    @foreach ($faculties_analize as $f)
                        <li class="list-group-item">{{ $f }}</li>
                    @endforeach
                </ul>
            --}}
                
            </div>
        </div>        
    </div>
@endsection