@extends('layouts.app')

@section('content')
  {{-- breadcrumb --}}
  <div class="container cit-content">
      <div class="row justify-content-center no-gutters">
          <div class="col-md-10">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb bg-transparent">
                <li class="breadcrumb-item"><a href={{ route('home') }} class="text-dark">Home</a></li>
                <li class="breadcrumb-item"><a href={{ route('administration') }} class="text-dark">Administración</a></li>
                <li class="breadcrumb-item"><a href={{ route('periodos') }} class="text-dark">Periodos</a></li>
                <li class="breadcrumb-item active" aria-current="page">Activar período</li>
              </ol>
            </nav>
          </div>
      </div>
    </div>
  {{-- flash message --}}
  <div class="container">
        <div class="row justify-content-center" style="height:50px;">
            <div class="col-md-8">
                @if(Session::has('message'))
                    <flashmessage-component message="{{ Session::get('message') }}"></flashmessage-component>
                @else
                    <flashmessage-component message="¡Períodos en academia!"></flashmessage-component>
                @endif
            </div>
        </div>
    </div>
    {{-- formulario periodo --}}
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Períodos</h5>
                        <p class="card-text">Al activar un período todas las operaciones se procesarán con el período activado.</p>
                        <form method="POST" action="{{ route('storePeriodo') }}">
                            @csrf
                            <div class="form-group mt-3">
                              <select name="nombre" class="custom-select" required>
                                <option value="">Seleccione el período...</option>
                                @foreach ($periodos as $periodo)
                                  <option value={{ $periodo }}>{{ $periodo }}</option>
                                @endforeach
                              </select>
                              @if ($errors->has('nombre'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('nombre') }}</strong>
                                </span>
                              @endif
                              <div class="invalid-feedback">Example invalid custom select feedback</div>
                            </div>
                            <div class="form-group mb-0 mt-5">
                                <button type="submit" class="btn btn-dark">Activar período</button>
                                <button type="button" class="btn btn-outline-dark">Cancelar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
