@extends('layouts.app')

@section('content')
  {{-- breadcrumb --}}
  <div class="container">
    <div class="row justify-content-center no-gutters">
        <div class="col-md-10">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent">
              <li class="breadcrumb-item"><a href={{ route('home') }}>Home</a></li>
              <li class="breadcrumb-item"><a href={{ route('administration') }}>Administración</a></li>
              <li class="breadcrumb-item active" aria-current="page">Período</li>
            </ol>
          </nav>
        </div>
    </div>
  </div>
  {{-- flash message --}}
  <div class="container">
    <div class="row justify-content-center" style="height:50px;">
            <div class="col-md-8">
                @if(Session::has('message'))
                    <flashmessage-component message="{{ Session::get('message') }}"></flashmessage-component>
                @else
                    <flashmessage-component message="¡Lista de períodos ok!"></flashmessage-component>
                @endif
            </div>
    </div>
  </div>
  {{-- boton nuevo --}}
  <div class="container">
    <div class="row align-items-center justify-content-center" style="height:70px;">
        <div class="col-md-auto">
            <a href="#" class="btn btn-primary btn-lg" tabindex="-1" role="button" aria-disabled="true">Nuevo</a>
        </div>
    </div>
  </div>
  {{-- tabla de periodos --}}
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
          <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col" style="width: 2%">#</th>
                        <th scope="col" style="width: 90%">Período</th>
                        <th scope="col" colspan="2" style="width: 8%">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @for ($i = 1; $i < 10; $i++)
                        <tr>
                            <th scope="row">{{ $i }}</th>
                            <td>2018-03</td>
                            <td class="actions"><a href="" class="icon"><i class="fas fa-edit"></i></a></td>
                            <td class="actions"><button-delete-component r={{ '#' }} e={{ '10' }}></button-delete-component></td>
                        </tr>
                    @endfor
                </tbody>
            </table>
          </div>
      </div>
  </div>
  </div>
@endsection