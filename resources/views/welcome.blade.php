<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>UAO Virtual - SGA</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #f8f8f8;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .cit-header{
                z-index: 10;
                padding: 1rem;
                box-shadow: 4px 4px 10px #ccc;
                position: fixed;
                width: 100%;
                background-color: white;
                top:0;
            }

            .cit-header-logo{
                max-width: 14rem;
            }

            .cit-welcome-container{
                display:flex;
                margin-top:3rem;
                flex-wrap:wrap;
            }

            .cit-welcome-container-text{
                margin-top:10rem;
            }

            .cit-welcome-card{      
                background-color: white;
                border-radius:20px;
                min-width:20rem;
                padding:1rem;
                box-shadow: 4px 4px 15px;
                margin-right:2rem;
                margin-bottom:2rem;
            }

            .cit-card-link{
                display:flex;
                flex-direction:column;
                align-items: center;
            }
            .cit-card-link:hover{
                text-decoration:none;
            }

            .cit-welcome-card-img{
                max-width:10rem;
            }

            .cit-welcome-card-text{
                padding: 0.5rem 1rem;
                margin: 1rem;
                border: 1px solid #c4001a;
                border-radius: 22px;
                color: #c4001a;
                background: white;
                transition: all 0.2s ease;
                font-size: 1.2rem;
            }

            .cit-welcome-card-text:hover{
                color: white;
                background: #c4001a;
            }
        </style>
    </head>
    <body>

        <header class="cit-header">
            <div class="container">
                <img class="cit-header-logo" src="{{ asset('img/logo.png') }}">
            </div>  
        </header>
        <div class="container cit-welcome-container-text">
            <h5>Bienvenido</h5>
            <h3>Sistema de Gestión Administrativo - UAO Virtual</h3>
        </div>
        <div class="container cit-welcome-container">
            <div class="cit-welcome-card">
            @if (Route::has('login'))
                    @auth
                        <a href="{{ route('home') }}" class="cit-card-link">
                            <img class="cit-welcome-card-img" src="{{ asset('img/home.png') }}">
                            <h4 class="cit-welcome-card-text">Ir a inicio</h4>
                         </a> 
                    @else
                        <a href="{{ route('login') }}" class="cit-card-link">
                            <img class="cit-welcome-card-img" src="{{ asset('img/login.png') }}">
                            <h4 class="cit-welcome-card-text">Iniciar sesión</h4>
                         </a> 
                    @endauth
            @endif
            </div>
            <div class="cit-welcome-card">
                <a href="{{ route('consultarGruposAsignaturaDocente') }}" class="cit-card-link">
                    <img class="cit-welcome-card-img" src="{{ asset('img/form.png') }}">
                    <h4 class="cit-welcome-card-text">Formato de solicitud</h4>
                </a> 
            </div>
        </div>
    </body>
</html>
