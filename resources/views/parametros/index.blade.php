@extends('layouts.app')

@section('content')
  {{-- breadcrumb --}}
  <div class="container cit-content">
    <div class="row justify-content-center no-gutters">
        <div class="col-md-10">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent">
              <li class="breadcrumb-item"><a href={{ route('home') }} class="text-dark">Home</a></li>
              <li class="breadcrumb-item"><a href={{ route('administration') }} class="text-dark">Administración</a></li>
              <li class="breadcrumb-item active" aria-current="page">Parámetros</li>
            </ol>
          </nav>
        </div>
    </div>
  </div>
  {{-- flash message --}}
  <div class="container">
    <div class="row justify-content-center" style="height:50px;">
            <div class="col-md-8">
                @if(Session::has('message'))
                    <flashmessage-component message="{{ Session::get('message') }}"></flashmessage-component>
                @else
                    <flashmessage-component message="¡Lista de parámetros ok!"></flashmessage-component>
                @endif
            </div>
    </div>
  </div>
  {{-- boton nuevo --}}
  <div class="container">
    <div class="row align-items-center justify-content-center" style="height:70px;">
        <div class="col-md-auto">
            <a href="{{ route('createParametro') }}" class="btn btn-dark" tabindex="-1" role="button" aria-disabled="true">nuevo</a>
        </div>
    </div>
  </div>
  {{-- tabla de periodos --}}
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
          <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col" style="width: 2%">#</th>
                        <th scope="col" style="width: 30%">Parámetro</th>
                        <th scope="col" style="width: 50%">Valor</th>
                        <th scope="col" colspan="2" style="width: 18%">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                  @php
                    $i=1;
                  @endphp
                    @foreach ($parametros as $parametro)
                        <tr>
                            <th scope="row">{{ $i }}</th>
                            <td>{{ $parametro->nombre }}</td>
                            <td>{{ $parametro->valor }}</td>
                            <td class="actions">
                                <a href={{ route('editParametro', ['nombre' => $parametro->nombre]) }} class="icon"><i class="fas fa-edit"></i></a>
                                <form action={{ action('ParametroController@destroy',$parametro->nombre) }} method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn" type="submit" style='background-color:transparent;'>
                                        <i class="fas fa-trash-alt" style="color:#007bff;"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @php
                          $i++;
                        @endphp                        
                    @endforeach
                </tbody>
            </table>
          </div>
      </div>
  </div>
  </div>
@endsection