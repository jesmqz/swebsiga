@extends('layouts.app')

@section('content')
  <div class="container cit-content">
    {{-- breadcrumb --}}
    <div class="row justify-content-center no-gutters">
      <div class="col-md-10">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent">
            <li class="breadcrumb-item"><a href={{ route('home') }} class="text-dark">Home</a></li>
            <li class="breadcrumb-item"><a href={{ route('administration') }} class="text-dark">Administración</a></li>
            <li class="breadcrumb-item"><a href={{ route('parametros') }} class="text-dark">Parámetros</a></li>
            <li class="breadcrumb-item active" aria-current="page">Actualizar parámetro</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>

  {{-- flash message --}}
  <div class="container">
    <div class="row justify-content-center" style="height:50px;">
        <div class="col-md-8">
            @if(Session::has('message'))
                <flashmessage-component message="{{ Session::get('message') }}"></flashmessage-component>
            @else
                <flashmessage-component message="¡Formulario para crear parámetros Ok!"></flashmessage-component>
            @endif
        </div>
    </div>
  </div>

    {{-- formulario parametros --}}

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <h5>Datos del parámetro</h5>
                        <form method="POST" action="{{ route('updateParametro', ['nombre' => $parametro['nombre'] ])}}">
                            @csrf
                            <div class="form-row mt-3">
                                <div class="form-group col">
                                    <input id="nombre" type="text" class="form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }}" name="nombre" value="{{ $parametro['nombre'] }}" required autofocus placeholder="Nombre">
                                    @if ($errors->has('nombre'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('nombre') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>    
                            <div class="form-row justify-content-start">
                                @if ($parametro['nombre'] == 'hora-exec-solicitudes' || $parametro['nombre'] == 'hora-exec-updates' || $parametro['nombre'] == 'hora-exec-unenrolls')
                                    <div class="form-group col-4 col-sm-3 col-lg-2">
                                        <select class="form-control" id="hour" name="hour">
                                            @for ($i = 0; $i <= 12; $i++)
                                                @php
                                                    $i < 10 ? $hour='0'.$i : $hour = $i
                                                @endphp
                                                @if ($hour == $parametro['valor'][0])
                                                    <option selected="selected">{{ $hour }}</option>    
                                                @else
                                                    <option>{{ $hour }}</option>    
                                                @endif
                                                
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="form-group col-4 col-sm-3 col-lg-2">
                                        <select class="form-control" id="minute" name="minute">
                                            @for ($i = 0; $i <= 59; $i++)
                                                @php
                                                    $i < 10 ? $minute='0'.$i : $minute = $i
                                                @endphp
                                                @if ($minute == $parametro['valor'][1])
                                                    <option selected="selected">{{ $minute }}</option>
                                                @else
                                                    <option>{{ $minute }}</option>    
                                                @endif
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="form-group col-4 col-sm-3 col-lg-2">
                                        <select class="form-control" id="amfm" name="amfm">
                                            @if ($parametro['ampm'] == 'am')
                                                <option selected="selected">am</option>
                                                <option>pm</option>
                                            @else
                                                <option>am</option>
                                                <option selected="selected">pm</option>
                                            @endif
                                        </select>
                                    </div>
                                @else
                                    <div class="form-group col">
                                        <input id="valor" type="text" class="form-control{{ $errors->has('valor') ? ' is-invalid' : '' }}" name="valor" value="{{ $parametro["valor"] }}" required autofocus placeholder="Valor">
                                    </div>
                                @endif

                                @if ($errors->has('valor'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('valor') }}</strong>
                                    </span>
                                @endif
                            </div>
                            @if ($parametro['nombre'] == 'hora-exec-solicitudes' || $parametro['nombre'] == 'hora-exec-updates' || $parametro['nombre'] == 'hora-exec-unenrolls')
                                <div class="form-row justify-content-center">
                                    <div class="form-group col-12 col-sm-3 col-lg-2">
                                        <div class="form-check">
                                            @if ($parametro['disabled'] == true)
                                                <input class="form-check-input" type="checkbox" value="true" id="disabled" name="disabled" checked>
                                                <label class="form-check-label" for="defaultCheck1">
                                                    Deshabilitar
                                                </label>
                                            @else
                                                <input class="form-check-input" type="checkbox" value="true" id="disabled" name="disabled">
                                                <label class="form-check-label" for="defaultCheck1">
                                                    Deshabilitar
                                                </label>
                                            @endif
                                                
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group mb-0 mt-5">
                                <button type="submit" class="btn btn-dark">Actualizar</button>
                                <a href={{ route('parametros') }} class="btn btn-outline-dark" role="button" aria-pressed="true">Cancelar</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
  </div>
@endsection