@extends('layouts.app')

@section('content')
  <div class="container cit-content">
    {{-- breadcrumb --}}
    <div class="row justify-content-center no-gutters">
      <div class="col-md-10">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent">
            <li class="breadcrumb-item"><a href={{ route('home') }} class="text-dark">Home</a></li>
            <li class="breadcrumb-item"><a href={{ route('administration') }} class="text-dark">Administración</a></li>
            <li class="breadcrumb-item"><a href={{ route('parametros') }} class="text-dark">Parámetros</a></li>
            <li class="breadcrumb-item active" aria-current="page">Crear parámetro</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>

  {{-- flash message --}}
  <div class="container">
    <div class="row justify-content-center" style="height:50px;">
        <div class="col-md-8">
            @if(Session::has('message'))
                <flashmessage-component message="{{ Session::get('message') }}"></flashmessage-component>
            @else
                <flashmessage-component message="¡Formulario para crear parámetros Ok!"></flashmessage-component>
            @endif
        </div>
    </div>
  </div>

    {{-- formulario parametros --}}

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <h5>Datos del parámetro</h5>
                        <form method="POST" action="{{ route('storeParametro')}}">
                            @csrf
                            <div class="form-row mt-3">
                                <div class="col">
                                    <input id="nombre" type="text" class="form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }}" name="nombre" value="{{ old('nombre') }}" required autofocus placeholder="Nombre">
                                    @if ($errors->has('nombre'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('nombre') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col">
                                    <input id="valor" type="text" class="form-control{{ $errors->has('valor') ? ' is-invalid' : '' }}" name="valor" value="{{ old('valor') }}" required autofocus placeholder="Valor">
                                    @if ($errors->has('valor'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('valor') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group mb-0 mt-5">
                                <button type="submit" class="btn btn-dark">Crear</button>
                                <button type="button" class="btn btn-outline-dark">Cancelar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

  </div>
@endsection