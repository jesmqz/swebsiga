<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::view('/', 'welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Rutas administracion
Route::get('/home/administration', 'HomeController@admin')->name('administration');
Route::get('/home/administration/users', 'HomeController@users')->name('users');
Route::get('/home/administration/users/create-user', 'HomeController@createUser')->name('createUser');
Route::delete('/home/administration/users/delete-user/{email}', 'HomeController@deleteUser')->name('deleteUser');
Route::get('/home/administration/users/edit-user/{email}', 'HomeController@editUser')->name('editUser');
Route::post('/home/administration/users/create-user', 'HomeController@saveUser')->name('saveUser');
Route::post('/home/administracion/users/update-user/{email}', 'HomeController@updateUser')->name('updateUser');
// Rutas solicitud
Route::resource('solicitud', 'SolicitudController', [
  'only' => ['create', 'store', 'edit', 'update', 'show', 'destroy']
]);
Route::get('/home/solicitudes/group/{id_group}', 'SolicitudController@showGroup')->name('solicitud.showGroup');
Route::delete('/home/solicitudes/group/{id_group}', 'SolicitudController@destroyGroup')->name('solicitud.destroyGroup');
Route::get('/home/solicitudes', 'SolicitudController@index')->name('solicitudes');
Route::get('/home/solicitudes/redirecttocampus/{shortname}', 'SolicitudController@redirectToCampus')->name('solicitud.redirectToCampus');
Route::get('/consultar-grupos-asignatura-docente','HomeController@consultarGruposAsignaturaDocente')->name('consultarGruposAsignaturaDocente');
Route::get('/ir-crear-solicitud','HomeController@redirigirACrearSolicitud')->name('redirigirCrearSolicitud');
// Rutas CRUD periodo
Route::get('/home/administration/periodos', 'PeriodoController@index')->name('periodos');
Route::get('/home/administration/periodos/create', 'PeriodoController@create')->name('createPeriodo');
Route::post('/home/administration/periodos', 'PeriodoController@store')->name('storePeriodo');

// Rutas CRUD parametros
Route::get('/home/administration/parametros', 'ParametroController@index')->name('parametros');
Route::get('/home/administration/parametros/create', 'ParametroController@create')->name('createParametro');
Route::post('/home/administration/parametros','ParametroController@store')->name('storeParametro');
Route::get('/home/administration/parametros/edit-parametro/{nombre}', 'ParametroController@edit')->name('editParametro');
Route::post('/home/administracion/parametros/update-parametro/{nombre}', 'ParametroController@update')->name('updateParametro');
Route::delete('/home/administration/parametros/delete-user/{nombre}', 'ParametroController@destroy')->name('deleteParametro');
Route::get('/reportes', 'GenerateReports')->name('reportes');